const moment = require('moment');
const Q = require('q');
const fs = require('fs');
var jwt = require('jsonwebtoken');
const config = require('../config/env.json')[ process.env.NODE_ENV || 'development' ];

exports.debug = function (message) {
    console.log(`[${moment().format("YYYY/MM/DD:HH:mm:ss.SSS Z")}] DEBUG: ${message}`)
}

exports.errorLog = function (message, header) {
    header = header || "ERROR";
    console.error(`[${moment().format("YYYY/MM/DD:HH:mm:ss.SSS Z")}] ${header}`);
    console.error(message);
}

exports.readTextFile = function (path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf-8', function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    })
}

exports.buildMongoDBUrl = function() {    
    return 'mongodb://' + 
        config.DB.host + ":" +
        config.DB.port + "/" +
        config.DB.name; 
}

exports.cloneObj = function(obj) {
    var sendback = [];
    if( ! obj ) {
        Object.keys(obj).forEach(function(key) {
            sendback[key] = obj[key];                    
        });
    }    
    return sendback;
}

exports.sendEmail = function(from, to, subject, content) {
    var deferred = Q.defer();

    try {
        var nodemailer = require('nodemailer');
        var smtpTransport = require('nodemailer-smtp-transport');
        var transport = nodemailer.createTransport(smtpTransport({
            service: config.MAIL.service,
            host: config.MAIL.host,
            auth: config.MAIL.account
        }));
        var mailOptions = {
            from: from,
            to: to,
            subject: subject,
            text: content
        };
        transport.sendMail(mailOptions, function(error, info) {		            
            if(error)
                deferred.reject(error.message || error);
            else
                deferred.resolve(info);                               
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

exports.generateError = function( message, code ) {
    return { messgae: message, code: code }
}

exports.generateToken = function( user ) {
    return jwt.sign({ role: user.role }, config.JWT.secret, {
        issuer : config.JWT.issuer,
        subject : user.fullname,
        algorithm: config.JWT.algorithm,
        expiresIn: config.JWT.expiresIn
    });
}

exports.verifyToken = function( token ) {
    var deferred = Q.defer();
    
    jwt.verify( token, config.JWT.secret, function(err, decoded){
        if(err) deferred.resolve({ error: "Failed to authenticate access token" })
        else deferred.resolve(decoded);       
    });

    return deferred.promise;
}

exports.render = function (req, res, view, params) {

    res.render(view, makeViewParameters(req, params));

}

function makeViewParameters(req, params) {

    var result = {

        user: req.user,

        moment: moment,

        baseUrl: req.baseUrl,
        
        pageUrl: req.originalUrl       

    }

    if (params) {

        for (var key in params) {

            result[key] = params[key]

        }

    }

    return result;

}