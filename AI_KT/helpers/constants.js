exports.MESSAGE_AUTH_FAILURE = "Invalid username or password";
exports.MESSAGE_USER_NOT_FOUND = "User not found";
exports.MESSAGE_NO_TOKEN = "No token provided";
exports.MESSAGE_INVALID_TOKEN = "Invalid access token";
exports.MESSAGE_MENU_NOT_FOUND = "Menu not found";