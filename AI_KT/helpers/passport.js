const passport = require('passport');
const Strategy = require('passport-local').Strategy;
const userSerivce = require('../services/user.service');

passport.use(new Strategy(
    function (username, password, done) {
        userSerivce.authenticateUser(username, password)
            .then(match => done(null, match))
            .catch(reason => {
                setTimeout(() => done(null, null, { message: reason.message || reason }), 1000)
            });
    }))

passport.serializeUser(function (user, done) {
    done(null, user)
})

passport.deserializeUser(function (user, done) {
    done(null, user)
})

module.exports = passport
