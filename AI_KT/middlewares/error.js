const utils = require('../helpers/utils')

module.exports = function (err, req, res, next) {
    if (!(err.status === 404 && req.originalUrl === "/favicon.ico")) {
        utils.errorLog(err)
    }    
    if (!res.headersSent) {
        res.status(err.status || 500)
        res.render('pages/errors', {
            message: err.message,
            error: err
        })
    }
}
