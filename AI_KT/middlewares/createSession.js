const express = require('express')
const session = require('express-session');
const uuid = require('uuid');
const config = require('../config/env.json')[process.env.NODE_ENV || 'local'];
const MAX_AGE = 24 * 60 * 60 * 1000

var middleware = undefined
module.exports = function (req, res, next) {

    if (!middleware) {
        var sessionOptions = {
            genid: req => uuid.v4(),
            secret: config.secret,
            saveUninitialized: false,
            resave: true,
            rolling: false,
            proxy: false,
            cookie: {
                path: '/',
                httpOnly: true,
                secure: false,
                maxAge: MAX_AGE
            }
        }
        middleware = session(sessionOptions)
    }
    middleware(req, res, next)
}

