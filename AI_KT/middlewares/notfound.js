module.exports = function (req, res, next) {
    var err = {
        message: "404 Not Found " + req.originalUrl,
        status: 404
    }
    next(err)
}
