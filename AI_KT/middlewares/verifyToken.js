var jwt = require('jsonwebtoken');
const constants = require('../helpers/constants');
const config = require('../config/env.json')[ process.env.NODE_ENV || 'development' ];

function verifyToken(req, res, next) {
  var authorization = req.headers.authorization;
  var access_token = authorization.split(' ')[1];
  if ( access_token ) {
    jwt.verify(access_token, config.JWT.secret, function(err, decoded) {
      if (err) return res.status(500).send({ error: constants.MESSAGE_INVALID_TOKEN });    
      console.log("decoded:" + JSON.stringify(decoded));                
      next();
    });
  } else {
    return res.status(403).json({ error: constants.MESSAGE_NO_TOKEN });  
  }       
}

module.exports = verifyToken;