module.exports = function (req, res, next) {
    require('crypto').randomBytes(64, function (ex, buf) {
        req.session.CSRFToken = buf.toString('hex')
        req.session.save(() => {
            next()
        })
    });
}
