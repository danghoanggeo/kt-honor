This project is a small project that support japanese learning system

Setup Mecab library:
1. Run container: docker exec -it container_id /bin/bash
2. Update: apt-get update && apt-get upgrade
3. Install initial base library: 
    3.1 apt-get install -y mecab libmecab-dev mecab-ipadic
    3.2 apt-get install -y mecab-ipadic-utf8
    3.3 apt-get install -y libc6-dev build-essential
4. exit container and install mecab: npm install mecab-async



Mongoldb dump: backup
1.  docker ps -a
2. docker exec -it mongodb_id /bin/bash 
3. mongodump --db writejapanese -o backup/
4. cd backup/
5. exit
6. docker cp 1f063f59716e:/backup/writejapanese db/


Mongoldb resort: update
1. docker exec -it mongodb_id  /bin/bash
2. mongorestore --db db_name db/writejapanese/
3.
