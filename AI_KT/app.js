// var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
var logger = require('morgan');
var app = express();
var utils = require('./helpers/utils')
var error = require('./middlewares/error')
var notfound = require('./middlewares/notfound')
var mqtt = require('mqtt');

// var client = mqtt.connect("mqtt://192.168.1.43",1883,{clientId:"60"})
// client.on("connect",function(){	
//   console.log("mqtt connected");
// });
// client.on('connect', function () {
//   client.subscribe('presence', function (err) {
//     if (!err) {
//       client.publish('presence', 'Hello mqtt')
//     }
//   })
// })

// client.on('message', function (topic, message) {
//   // message is Buffer
//   console.log(message.toString())
//   client.end()
// })
require('bluebird').Promise.config({
  warnings: false,
  longStackTraces: true,
  cancellation: false,
  monitoring: false
})

const rawBody = function (req, res, buf) {
  if (buf && buf.length) {
      req.rawBody = buf.toString('utf8');
  }
}

var mongoose = require ( 'mongoose' );
mongoose.connect(utils.buildMongoDBUrl(), { useCreateIndex: true, useNewUrlParser: true });
// With object options
// mongoose.createConnection(utils.buildMongoDBUrl(), { poolSize: 4 });

// app.set('views', path.join(__dirname, 'frontend'));
// app.engine('html', require('ejs').renderFile);
// app.set('view engine', 'html');

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger('dev'));
app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb', parameterLimit: 1000000 }))
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', require('./routes/index'));

app.use('/admin-lte', express.static(__dirname + '/node_modules/admin-lte/'));
app.use('/js', express.static(__dirname + '/node_modules/angular'));
app.use('/js', express.static(__dirname + '/node_modules/angular-cookies'));
app.use('/mqtt', express.static(__dirname + '/node_modules/mqtt'));


app.use(notfound)
app.use(error)

utils.debug(`launched in ${process.env.NODE_ENV || 'development'} mode`)

module.exports = app

module.exports = app;
