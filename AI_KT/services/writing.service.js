var _ = require('lodash');
var Q = require('q');
var MeCab = new require('mecab-async');
const MESSAGE_NOT_FOUND = "Writing not found";
const utils = require('../helpers/utils');
const jsonData = require('../db/writing_webapp/sentences.json');
var service = {};

service.createWriting = createWriting;
service.updateWriting = updateWriting;
service.getAllWritings = getAllWritings;
service.getAllSentences = getAllSentences;
service.getWritingsByUser = getWritingsByUser;
service.createListWriting = createListWriting;
service.getWritingById = getWritingById;
service.removeWriting = removeWriting;
service.analyzeWriting = analyzeWriting;

module.exports = service;

Writing = require('../models/writing.model');


// get the writing by its ID
function getWritingById(_id) {
    var deferred = Q.defer();

    try {
        Writing.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

// Create a new writing 

function createWriting(obj) {

    var deferred = Q.defer();

    try {
        // validation
        Writing.findOne({ text: obj.text }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc ) {
                deferred.reject('Writing "' + obj.text + '" is already taken');
            } else {
                var writing = new Writing({});

                Object.keys(obj).forEach(function (key) {
                    writing[key] = obj[key];
                });
              
                writing.save((err, doc)=> {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    Writing.find({ 'done': false }, (err, writings)=>{
                        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                        if (err)
                            deferred.reject(err.name + ': ' + err.message);
                        // docs is an array
                        if (writings) {
                            deferred.resolve(writings);
                        } else {
                            deferred.reject(MESSAGE_NOT_FOUND);
                        }
                    });
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


//Create writings list
function createListWriting(listObjs) {

    var deferred = Q.defer();
    try {
        Writing.insertMany(listObjs, function (error, docs) {
            if (error) deferred.reject(error);
            else deferred.resolve(docs);
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


// Update writing by object
function updateWriting(obj) {
    var deferred = Q.defer();
    var orgDoc = {};
    try {
        // validation
        Writing.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            orgDoc = doc;
            if (doc.Writing_jp !== obj.Writing_jp) {
                Writing.findOne({ Writing_jp: obj.Writing_jp }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Writing "' + doc.Writing_jp + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            var sendback = {};
            Writing.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, result) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                else {
                    sendback.Writing = result;
                    Content.update(
                        { "Writings.Writing_jp": orgDoc.Writing_jp },
                        {
                            $set:
                            {
                                "Writings.$.Writing_jp": set.Writing_jp,
                                "Writings.$.Writing_vi": set.Writing_vi,
                                "Writings.$.audio": set.audio,
                                "Writings.$.updated_date": set.updated_date
                            }
                        },
                        { multi: true }
                    ).exec(
                        function (err, result) {
                            if (err) {
                                Writing.update({ _id: orgDoc._id }, { $set: _.omit(set, '_id') }, function (err, result) {
                                    if (err) console.log(err);
                                    else console.log(result);
                                })
                                deferred.reject(err.name + ': ' + err.message);
                            } else {
                                sendback.content = result;
                                deferred.resolve(sendback);
                            }
                        }
                    );
                }
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }


    return deferred.promise;
}

// Get all writings

function getAllWritings() {
    var deferred = Q.defer();

    try {
        // use mongoose to get all writings in the database
        Writing.find({ 'done': false }, (err, writings)=>{
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                deferred.reject(err.name + ': ' + err.message);
            // docs is an array
            if (writings) {
                deferred.resolve(writings);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getAllSentences(){
    var deferred = Q.defer();
    try{
        deferred.resolve(jsonData);
    }catch(ex){
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function getWritingsByUser(username) {
    var deferred = Q.defer();

    try {
        Writing.findOne({ created_by: username }).sort({ order: 1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

// remove a writing

function removeWriting(writingId){
    var deferred = Q.defer();
    try{
        Writing.remove({
            _id : writingId
        }, function(err, writing) {
            if (err)
                deferred.reject(err);
            Writing.find({ 'done': false }, (err, writings)=>{
                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                if (err)
                    deferred.reject(err.name + ': ' + err.message);
                // docs is an array
                if (writings) {
                    deferred.resolve(writings);
                } else {
                    deferred.reject(MESSAGE_NOT_FOUND);
                }
            });
        });
    }catch(ex){
        deferred.reject(ex.message || ex);
    }
    deferred.promise; 
}

function checkSentencesMistakes(originalSen,userSen){
    var index = 0;
    result = {};
    for (var word in userSen) {
        var flag = false;
        for(jdex = index;jdex<originalSen.length;jdex++){
            if(userSen[word] == originalSen[jdex]){
                flag = true;
                index ++;
            }
        }
        if(flag){
            result[word] = {"word":userSen[word],"valid":true};
        }else{
            result[word] = {"word":userSen[word],"valid":false};
        }
    }
    return result;
}

// use Mecab library to analyze a sentence
function analyzeWriting(index,sentence){
    var deferred = Q.defer();
    var originalSentence = jsonData[index]["jp"];
    try{
        var mecab = new MeCab()
        mecab.wakachi(sentence, (err, result0)=>{
            if (err){
                console.log(err);
                deferred.reject(err);
            }else{
                mecab.wakachi(originalSentence, (err, result1)=>{
                    //var result = {"newSen":result0,"oldSen":result1};
                    var result = checkSentencesMistakes(result1,result0);
                    deferred.resolve(result);
                });
            }
        });
    }
    catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

