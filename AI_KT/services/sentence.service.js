var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "Sentence not found";
const utils = require('../helpers/utils');

var service = {};

service.createSentence = createSentence;
service.updateSentence = updateSentence;
service.deleteSentenceBySentenceJP = deleteSentenceBySentenceJP;
service.getAllSentences = getAllSentences;
service.getPaginationSentences = getPaginationSentences;
service.getSentencesByUser = getSentencesByUser;
service.getPaginationSentencesByUser = getPaginationSentencesByUser;
service.createListSentence = createListSentence;
service.getSentenceById = getSentenceById;
service.getSentenceBySentenceJP = getSentenceBySentenceJP

module.exports = service;

Content = require('../models/content.model');
Sentence = require('../models/sentence.model');

function getSentenceById(_id) {
    var deferred = Q.defer();

    try {
        Sentence.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
function getSentenceBySentenceJP(value) {
    var deferred = Q.defer();
    try {
        // validation
        Sentence.findOne({ sentence_jp: value }, function (err, doc) {
            // if (err) deferred.reject(err.name + ': ' + err.message);
            if (err) {
                deferred.resolve();
            }
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function createSentence(obj) {

    var deferred = Q.defer();

    try {
        // validation
        Sentence.findOne({ sentence_jp: obj.sentence_jp }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc ) {
                deferred.reject('Sentence "' + obj.sentence_jp + '" is already taken');
            } else {
                var sentence = new Sentence({});

                Object.keys(obj).forEach(function (key) {
                    sentence[key] = obj[key];
                });
              
                sentence.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function createListSentence(listObjs) {

    var deferred = Q.defer();
    try {
        Sentence.insertMany(listObjs, function (error, docs) {
            if (error) deferred.reject(error);
            else deferred.resolve(docs);
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function updateSentence(obj) {
    var deferred = Q.defer();
    var orgDoc = {};
    try {
        // validation
        Sentence.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            orgDoc = doc;
            if (doc.sentence_jp !== obj.sentence_jp) {
                Sentence.findOne({ sentence_jp: obj.sentence_jp }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Sentence "' + doc.sentence_jp + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            var sendback = {};
            Sentence.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, result) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                else {
                    sendback.sentence = result;
                    Content.update(
                        { "sentences.sentence_jp": orgDoc.sentence_jp },
                        {
                            $set:
                            {
                                "sentences.$.sentence_jp": set.sentence_jp,
                                "sentences.$.sentence_vi": set.sentence_vi,
                                "sentences.$.audio": set.audio,
                                "sentences.$.updated_date": set.updated_date
                            }
                        },
                        { multi: true }
                    ).exec(
                        function (err, result) {
                            if (err) {
                                Sentence.update({ _id: orgDoc._id }, { $set: _.omit(set, '_id') }, function (err, result) {
                                    if (err) console.log(err);
                                    else console.log(result);
                                })
                                deferred.reject(err.name + ': ' + err.message);
                            } else {
                                sendback.content = result;
                                deferred.resolve(sendback);
                            }
                        }
                    );
                }
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }


    return deferred.promise;
}

function deleteSentenceBySentenceJP(sentenceJP) {
    var deferred = Q.defer();
    try {
        Content.find({ sentences: { $elemMatch: { sentence_jp: sentenceJP } } }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc.length > 0) {
                deferred.reject("The sentence '" + sentenceJP + "' already exists in content. Can't delete this sentence");
            } else {
                Sentence.remove({ sentence_jp: sentenceJP }, function (err) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    else deferred.resolve({ sentence_jp: sentenceJP });
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getAllSentences() {
    var deferred = Q.defer();

    try {
        Sentence.find({}).sort({ created_date: -1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationSentences(skip, limit) {
    var deferred = Q.defer();

    try {
        Sentence.aggregate([
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getSentencesByUser(username) {
    var deferred = Q.defer();

    try {
        Sentence.find({ created_by: username }).sort({ order: 1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationSentencesByUser(username, skip, limit) {
    var deferred = Q.defer();

    try {
        Sentence.aggregate([
            { '$match': { 'created_by': username } },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
