var _ = require('lodash');
var Q = require('q');
var MeCab = new require('mecab-async');
const MESSAGE_NOT_FOUND = "test not found";
const utils = require('../../helpers/utils');
var service = {};

service.createTestGimonshi = createTestGimonshi;
service.createListtestGimonshi = createListtestGimonshi;
service.updatetestGimonshi = updatetestGimonshi;
service.gettestGimonshiById = gettestGimonshiById;
service.getAlltestGimonshiByListId = getAlltestGimonshiByListId;
service.checkAnswerById = checkAnswerById;
service.getAlltestGimonshi = getAlltestGimonshi;
service.removetestGimonshi = removetestGimonshi;
//service.gettestGimonshiByLesson = gettestGimonshiByLesson;
//service.gettestGimonshiByLevel = gettestGimonshiByLevel;
//service.gettestGimonshiByCorrectRate = gettestGimonshiByCorrectRate;


module.exports = service;

TestGimonshi = require('../../models/test/test.gimonshi.model');



// Create a new createtestGimonshi 

function createTestGimonshi(obj) {

    var deferred = Q.defer();

    try {
        // validation
        TestGimonshi.findOne({ quiz: obj.quiz }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc ) {
                deferred.reject('TestGimonshi "' + obj.quiz + '" is already taken');
            } else {
                var testGimonshi = new TestGimonshi({});

                Object.keys(obj).forEach(function (key) {
                    testGimonshi[key] = obj[key];
                });
                console.log(testGimonshi);
                testGimonshi.save((err, doc)=> {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    TestGimonshi.find({ 'status': true }, (err, testGimonshis)=>{
                        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                        if (err)
                            deferred.reject(err.name + ': ' + err.message);
                        // docs is an array
                        if (testGimonshis) {
                            deferred.resolve(testGimonshis);
                        } else {
                            deferred.reject(MESSAGE_NOT_FOUND);
                        }
                    });
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


//Create testGimonshi list
function createListtestGimonshi(listObjs) {

    var deferred = Q.defer();
    try {
        TestGimonshi.insertMany(listObjs, function (error, docs) {
            if (error) deferred.reject(error);
            else deferred.resolve(docs);
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


// Update writing by object
function updatetestGimonshi(obj) {
    var deferred = Q.defer();
    var orgDoc = {};
    try {
        // validation
        TestGimonshi.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            orgDoc = doc;
            if (doc.quiz !== obj.quiz) {
                TestGimonshi.findOne({ quiz: obj.quiz }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('TestGimonshi "' + doc.quiz + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            var sendback = {};
            TestGimonshi.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, result) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                else {
                    sendback.TestGimonshi = result;
                    Content.update(
                        { "TestGimonshi.quiz": orgDoc.quiz },
                        {
                            $set:
                            {
                                "TestGimonshi.$.answer": set.answer,
                                "TestGimonshi.$.num_used": set.num_used,
                                "TestGimonshi.$.num_correct": set.num_correct,
                                "TestGimonshi.$.num_explain":set.explain,
                                "TestGimonshi.$.level": set.level,
                                "TestGimonshi.$.lesson": set.lesson,
                                "TestGimonshi.$.created_by": set.created_by,
                                "TestGimonshi.$.updated_date": set.updated_date,
                                "TestGimonshi.$.status": set.status
                            }
                        },
                        { multi: true }
                    ).exec(
                        function (err, result) {
                            if (err) {
                                TestGimonshi.update({ _id: orgDoc._id }, { $set: _.omit(set, '_id') }, function (err, result) {
                                    if (err) console.log(err);
                                    else console.log(result);
                                })
                                deferred.reject(err.name + ': ' + err.message);
                            } else {
                                sendback.content = result;
                                deferred.resolve(sendback);
                            }
                        }
                    );
                }
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }


    return deferred.promise;
}

// get the testGimonshi by its ID
function gettestGimonshiById(_id) {
    var deferred = Q.defer();

    try {
        TestGimonshi.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

// get the all testGimonshi by its list ID
function getAlltestGimonshiByListId(listId) {
    var deferred = Q.defer();
    var result = {};
    try {
        for(var _id in listId){
            TestGimonshi.findById(_id, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                if (doc) {
                    result[_id]["quiz"] = doc.quiz;
                    result[_id]["numAnswer"] = len(doc.answer);
                }
            });
        }
        deferred.resolve(result);
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


// check the Answer by its ID
// listAnswers = {id1:answer,id2:answer}
function checkAnswerById(listAnswers) {
    var deferred = Q.defer();
    try {
        // Loop throgh all answers of user's submit
        var result = {};
        var correct = 0;
        retu = 0;
        var size = 0;
        console.log(listAnswers);
        for(_id in listAnswers){
            TestGimonshi.findById(_id, function (err, testGimonshi) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                if (testGimonshi) {
                    id = testGimonshi._id;
                    answer = listAnswers[id];// Object
                    //console.log(answers);
                    correctAnswers = testGimonshi["answer"]; // array
                    //console.log(correctAnswers);
                    var flag = true;
                    var checkAnswer = {};
                    if(correctAnswers!= answer){
                        checkAnswer=  {"ans":correctAnswers,"uAns":answer,"isCorrect":false};      //wrong answer
                        flag = false;
                    }else{                  // else increase number of corret answer
                        checkAnswer ={"ans":correctAnswers,"isCorrect":true}; // correct answer
                    }
                    // response result with some property
                    result[id] = {"_id":id,result:checkAnswer,"explain":testGimonshi.explain};
                    if(flag){
                        correct ++;    // TODO: update number of correct answer to database
                    }
                    retu++;
                    if(retu == size){
                        result["numCorrect"]=correct;
                        result["percent"] = (correct/retu)*100;
                        console.log(result);
                        deferred.resolve(result);
                    }
                }

            });
            size ++;
        }
        
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


// Get all getAlltestGimonshi

function getAlltestGimonshi(levelId) {
    var deferred = Q.defer();

    try {
        // use mongoose to get all testGimonshinas in the database
        TestGimonshi.find({ 'status': true,'level':levelId }, (err, testGimonshis)=>{
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                deferred.reject(err.name + ': ' + err.message);
            // docs is an array
            if (testGimonshis) {
                deferred.resolve(testGimonshis);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

// remove a test

function removetestGimonshi(data){
    var deferred = Q.defer();
    try{
        Id = data.test;
        levelId = data.level;
        TestGimonshi.deleteOne({
            _id : Id
        }, function(err, res) {
            if (err)
                deferred.reject(err);
                
                TestGimonshi.find({  'status': true,'level':levelId }, (err, TestGimonshis)=>{
                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                if (err)
                    deferred.reject(err.name + ': ' + err.message);
                // docs is an array
                if (TestGimonshis) {
                    deferred.resolve(TestGimonshis);
                } else {
                    deferred.reject(MESSAGE_NOT_FOUND);
                }
            });
        });
    }catch(ex){
        deferred.reject(ex.message || ex);
    }
    deferred.promise; 
}
