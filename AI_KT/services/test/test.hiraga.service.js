var _ = require('lodash');
var Q = require('q');
var MeCab = new require('mecab-async');
const MESSAGE_NOT_FOUND = "test not found";
const utils = require('../../helpers/utils');
var service = {};

service.createTestHiraga = createTestHiraga;
service.createListTestHiraga = createListTestHiraga;
service.updateTestHiraga = updateTestHiraga;
service.getTestHiragaById = getTestHiragaById;
service.getAllTestHiragaByListId = getAllTestHiragaByListId;
service.checkAnswerById = checkAnswerById;
service.getAllTestHiraga = getAllTestHiraga;
service.removeTestHiragana = removeTestHiragana;
//service.getTestHiragaByLesson = getTestHiragaByLesson;
//service.getTestHiragaByLevel = getTestHiragaByLevel;
//service.getTestHiragaByCorrectRate = getTestHiragaByCorrectRate;


module.exports = service;

TestHiragaHitotsu = require('../../models/test/test.hiraga.model');



// Create a new createTestHiraga 

function createTestHiraga(obj) {

    var deferred = Q.defer();

    try {
        // validation
        TestHiragaHitotsu.findOne({ quiz: obj.quiz }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc ) {
                deferred.reject('TestHiragaHitotsu "' + obj.quiz + '" is already taken');
            } else {
                var testHiragaHitotsu = new TestHiragaHitotsu({});

                Object.keys(obj).forEach(function (key) {
                    testHiragaHitotsu[key] = obj[key];
                });
                console.log(testHiragaHitotsu);
                testHiragaHitotsu.save((err, doc)=> {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    TestHiragaHitotsu.find({ 'status': true }, (err, testHiragaHitotsus)=>{
                        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                        if (err)
                            deferred.reject(err.name + ': ' + err.message);
                        // docs is an array
                        if (testHiragaHitotsus) {
                            deferred.resolve(testHiragaHitotsus);
                        } else {
                            deferred.reject(MESSAGE_NOT_FOUND);
                        }
                    });
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


//Create TestHiraga list
function createListTestHiraga(listObjs) {

    var deferred = Q.defer();
    try {
        TestHiragaHitotsu.insertMany(listObjs, function (error, docs) {
            if (error) deferred.reject(error);
            else deferred.resolve(docs);
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


// Update writing by object
function updateTestHiraga(obj) {
    var deferred = Q.defer();
    var orgDoc = {};
    try {
        // validation
        TestHiragaHitotsu.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            orgDoc = doc;
            if (doc.quiz !== obj.quiz) {
                TestHiragaHitotsu.findOne({ quiz: obj.quiz }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('TestHiragaHitotsu "' + doc.quiz + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            var sendback = {};
            TestHiragaHitotsu.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, result) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                else {
                    sendback.TestHiragaHitotsu = result;
                    Content.update(
                        { "TestHiragaHitotsu.quiz": orgDoc.quiz },
                        {
                            $set:
                            {
                                "TestHiragaHitotsu.$.answer": set.answer,
                                "TestHiragaHitotsu.$.num_used": set.num_used,
                                "TestHiragaHitotsu.$.num_correct": set.num_correct,
                                "TestHiragaHitotsu.$.num_explain":set.explain,
                                "TestHiragaHitotsu.$.level": set.level,
                                "TestHiragaHitotsu.$.lesson": set.lesson,
                                "TestHiragaHitotsu.$.wrongAnswers":set.wrongAnswers,
                                "TestHiragaHitotsu.$.created_by": set.created_by,
                                "TestHiragaHitotsu.$.updated_date": set.updated_date,
                                "TestHiragaHitotsu.$.status": set.status
                            }
                        },
                        { multi: true }
                    ).exec(
                        function (err, result) {
                            if (err) {
                                TestHiragaHitotsu.update({ _id: orgDoc._id }, { $set: _.omit(set, '_id') }, function (err, result) {
                                    if (err) console.log(err);
                                    else console.log(result);
                                })
                                deferred.reject(err.name + ': ' + err.message);
                            } else {
                                sendback.content = result;
                                deferred.resolve(sendback);
                            }
                        }
                    );
                }
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }


    return deferred.promise;
}

// get the TestHiraga by its ID
function getTestHiragaById(_id) {
    var deferred = Q.defer();

    try {
        TestHiragaHitotsu.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

// get the all TestHiraga by its list ID
function getAllTestHiragaByListId(listId) {
    var deferred = Q.defer();
    var result = {};
    try {
        for(var _id in listId){
            TestHiragaHitotsu.findById(_id, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                if (doc) {
                    result[_id]["quiz"] = doc.quiz;
                    result[_id]["numAnswer"] = len(doc.answer);
                }
            });
        }
        deferred.resolve(result);
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


// check the Answer by its ID
// listAnswers = {id1:[no,ga,ni],id2:[]}
function checkAnswerById(listAnswers) {
    var deferred = Q.defer();
    try {
        // Loop throgh all answers of user's submit
        var result = {};
        var correct = 0;
        retu = 0;
        var size = 0;
        console.log(size);
        for(_id in listAnswers){
            TestHiragaHitotsu.findById(_id, function (err, testHiraga) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                if (testHiraga) {
                    id = testHiraga._id;
                    answers = listAnswers[id];// Object
                    //console.log(answers);
                    correctAnswers = testHiraga["answer"]; // array
                    //console.log(correctAnswers);
                    var index = 0;
                    var flag = true;
                    var checkAnswer = {};
                    for(var answer in answers){
                        console.log("-----------------");
                        //TODO: If answer of users is wrong, save it to database
                        if(correctAnswers[index]!= answers[answer]){
                            checkAnswer[index]=  {"ans":correctAnswers[index],"uAns":answers[answer],"isCorrect":false};      //wrong answer
                            flag = false;
                        }else{                  // else increase number of corret answer
                            checkAnswer[index] ={"ans":correctAnswers[index],"isCorrect":true}; // correct answer
                        }
                        index ++;
                    }
                    // response result with some property
                    result[id] = {"_id":id,result:checkAnswer,"explain":testHiraga.explain};
                    if(flag){
                        correct ++;    // TODO: update number of correct answer to database
                    }
                    retu++;
                    if(retu == size){
                        result["numCorrect"]=correct;
                        result["percent"] = (correct/retu)*100;
                        console.log(result);
                        deferred.resolve(result);
                    }
                }

            });
            size ++;
        }
        
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


// Get all getAllTestHiraga

function getAllTestHiraga(levelId) {
    var deferred = Q.defer();

    try {
        // use mongoose to get all TestHiraganas in the database
        TestHiragaHitotsu.find({ 'status': true,'level':levelId }, (err, testHiragas)=>{
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                deferred.reject(err.name + ': ' + err.message);
            // docs is an array
            if (testHiragas) {
                deferred.resolve(testHiragas);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

// remove a test

function removeTestHiragana(data){
    var deferred = Q.defer();
    try{
        Id = data.test;
        levelId = data.level;
        TestHiragaHitotsu.deleteOne({
            _id : Id
        }, function(err, res) {
            if (err)
                deferred.reject(err);
                
                TestHiragaHitotsu.find({  'status': true,'level':levelId }, (err, testHiragaHitotsus)=>{
                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                if (err)
                    deferred.reject(err.name + ': ' + err.message);
                // docs is an array
                if (testHiragaHitotsus) {
                    deferred.resolve(testHiragaHitotsus);
                } else {
                    deferred.reject(MESSAGE_NOT_FOUND);
                }
            });
        });
    }catch(ex){
        deferred.reject(ex.message || ex);
    }
    deferred.promise; 
}
