var _ = require('lodash');
var Q = require('q');
var mongoose = require('mongoose');
const MESSAGE_NOT_FOUND = "Lesson not found";

var service = {};

service.createLesson = createLesson;
service.updateLesson = updateLesson;
service.deleteLessonById = deleteLessonById;
service.getAllLessons = getAllLessons;
service.getPaginationLessons = getPaginationLessons;
service.getLessonsByUser = getLessonsByUser;
service.getLessonById = getLessonById;
service.getPaginationLessonsByUser = getPaginationLessonsByUser;
service.getLessonByName = getLessonByName;
service.getLessonsByLevel = getLessonsByLevel;
module.exports = service;

Lesson = require('../models/lesson.model');
Content = require('../models/content.model');

function getLessonById(_id) {
    var deferred = Q.defer();

    try {
        Lesson.aggregate([
            { $match: { "_id": mongoose.Types.ObjectId(_id) } },
            {
                $lookup: {
                    from: "contents",  // collection name in db                
                    localField: "_id",
                    foreignField: "lesson",
                    as: "contents"
                }
            },
            { '$sort': { 'created_date': -1 } },
        ]).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs[0] || docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
function getLessonByName(name) {
    var deferred = Q.defer();

    try {
        Lesson.aggregate([
            { $match: { "name": name } },
            {
                $lookup: {
                    from: "contents",  // collection name in db                
                    localField: "_id",
                    foreignField: "lesson",
                    as: "contents"
                }
            },
            { '$sort': { 'created_date': -1 } },
        ]).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs[0] || docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });

    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function getLessonsByLevel(level) {
    var deferred = Q.defer();

    try {
        Lesson.aggregate([
            { $match: { "level": level } },
            {
                $lookup: {
                    from: "contents",  // collection name in db                
                    localField: "_id",
                    foreignField: "lesson",
                    as: "contents"
                }
            },
            { '$sort': { 'created_date': -1 } },
        ]).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve( docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });

    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;

}

function createLesson(obj) {

    var deferred = Q.defer();

    try {
        // validation
        Lesson.findOne({ name: obj.name }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc) {
                deferred.reject('Lesson "' + obj.name + '" is already taken');
            } else {
                var lesson = new Lesson({});

                Object.keys(obj).forEach(function (key) {
                    lesson[key] = obj[key];
                });

                lesson.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            }
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function updateLesson(obj) {
    var deferred = Q.defer();

    try {
        // validation
        Lesson.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc.name !== obj.name) {
                Lesson.findOne({ name: obj.name }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Lesson "' + doc.name + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            Lesson.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve(doc);
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }


    return deferred.promise;
}

// function deleteLessonById(_id) {
//     var deferred = Q.defer();

//     try {
//         Lesson.remove( { _id: _id }, function (err) {
//             if (err) deferred.reject(err.name + ': ' + err.message);    
//             deferred.resolve({ id: _id });
//         });
//     } catch (ex) {
//         deferred.reject( ex.message || ex );
//     }    

//     return deferred.promise;
// }

function deleteLessonById(_id) {
    var deferred = Q.defer();

    try {
        Content.find({lesson:_id }, function (err, docs) {
            console.log(docs);
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (docs.length>0) {
                deferred.reject("This lesson has content. Can't delete this lesson");
            } else {
                Lesson.remove({ _id: _id}, function (err) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve({ id: _id });
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getAllLessons() {
    var deferred = Q.defer();

    try {
        Lesson.aggregate([
            {
                $lookup: {
                    from: "contents",  // collection name in db                
                    localField: "_id",
                    foreignField: "lesson",
                    as: "contents"
                }
            },
            { '$sort': { 'created_date': -1 } },
        ]).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });

    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


function getPaginationLessons(skip, limit) {
    var deferred = Q.defer();

    try {
        Lesson.aggregate([
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getLessonsByUser(username) {
    var deferred = Q.defer();

    try {
        Lesson.find({ created_by: username }).sort({ order: 1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationLessonsByUser(username, skip, limit) {
    var deferred = Q.defer();

    try {
        Lesson.aggregate([
            { '$match': { 'created_by': username } },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}