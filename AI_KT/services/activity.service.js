var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "Activity not found";

var service = {};

service.createOrUpdateActivity = createOrUpdateActivity;
service.confirmActivity = confirmActivity;
service.getActivityById = getActivityById;
service.deleteActivityById = deleteActivityById;
service.getAllActivities = getAllActivities;
service.getActivitiesByUser = getActivitiesByUser;

module.exports = service;

Activity = require('../models/activity.model');
function createOrUpdateActivity(obj) {
    var deferred = Q.defer();
    try {
        Activity.findOne( { $and: [ {name: obj.name}, 
                                    {user: obj.user}, 
                                    {description: obj.description}, 
                                    {created_by: obj.created_by} ] 
                        },  function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if(doc)    
                update(); 
            else {
                // validation
                var activity = new Activity({});

                Object.keys(obj).forEach(function (key) {
                    activity[key] = obj[key];
                });
        
                activity.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            }
        });

        function update() {  
            var set = {};
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            set.confirmed = false;
            set.updated_date = Date.now();
            
            Activity.update({ $and: [ {name: obj.name}, 
                                      {user: obj.user}, 
                                      {description: obj.description}, 
                                      {created_by: obj.created_by} ] 
                            }, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);                    
                else deferred.resolve(doc);                
            });
        }

    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function confirmActivity(obj) {
    var deferred = Q.defer();
    try {
        // validation
        Activity.findOne( { _id: obj._id }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);    
            else 
                confirm(); 
        });

        function confirm() {  
            var set = {};
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            set.viewed = true;
            
            Activity.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);                    
                else deferred.resolve(doc);                
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function getActivityById(_id) {
    var deferred = Q.defer();
    try {
        // validation
        Activity.findOne( { _id: _id }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);    
            else deferred.resolve(doc);
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    } 
    return deferred.promise; 
}

function deleteActivityById(_id) {
    var deferred = Q.defer();

    try {
        Activity.remove({ _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getAllActivities() {
    var deferred = Q.defer();
    try {
        Activity.find().exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            } else if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getActivitiesByUser(user) {
    var deferred = Q.defer();

    try {
        Activity.find({user: user}).sort({"created_date": -1}).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            } else if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
