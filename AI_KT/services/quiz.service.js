var _ = require('lodash');
var Q = require('q');
var mongoose = require('mongoose');
const utils = require('../helpers/utils');
const MESSAGE_NOT_FOUND = "Quiz not found";
var parser = require('subtitles-parser');

var service = {};

service.getQuizById=getQuizById;
service.createQuiz = createQuiz;
service.updateQuiz = updateQuiz;
service.deleteQuizById = deleteQuizById;
service.getAllQuizes = getAllQuizes;
service.getPaginationQuizes = getPaginationQuizes;
service.getQuizesByUser = getQuizesByUser;
service.getPaginationQuizesByUser = getPaginationQuizesByUser;

module.exports = service;

Quiz = require('../models/quiz.model');
Sentence = require('../models/sentence.model');

function getQuizById(_id) {
    var deferred = Q.defer();

    try {
        Quiz.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc[0] || doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function createQuiz(obj) {
    var deferred = Q.defer();
    try {
        // validation
        Quiz.findOne({ name: obj.question_jp }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.reject('Quiz "' + obj.name + '" is already taken');
            } else {
                //TODO: need to fix by me                            
    
                    var quiz = new Quiz({});
                    Object.keys(obj).forEach(function (key) {
                        quiz[key] = obj[key];
                    });
                    quiz.save(function (err, doc) {
                        if (err) deferred.reject(err.name + ': ' + err.message);
                        else deferred.resolve(doc);
                    });
            }
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function updateQuiz(obj) {
    var deferred = Q.defer();
    try {
        // validation
        Quiz.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc.name !== obj.question_jp) {
                Quiz.findOne({ name: obj.question_jp }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Quiz "' + doc.name + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
                    Object.keys(obj).forEach(function (key) {
                    set[key] = obj[key];
                });
                Quiz.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);                    
                    else deferred.resolve(doc);                
                });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }


    return deferred.promise;
}

function deleteQuizById(_id) {
    var deferred = Q.defer();

    try {
        Quiz.remove({ _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getAllQuizes() {
    var deferred = Q.defer();

    try {
        Quiz.find({}).sort({ created_date: -1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getQuizesByUser(username) {
    var deferred = Q.defer();

    try {
        Quiz.find({ created_by: username }).sort({ order: 1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationQuizes(skip, limit) {
    var deferred = Q.defer();

    try {
        Quiz.aggregate([
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationQuizesByUser(username, skip, limit) {
    var deferred = Q.defer();

    try {
        Content.aggregate([
            { '$match': { 'created_by': username } },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
