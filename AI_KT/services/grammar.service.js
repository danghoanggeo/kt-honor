var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "Grammar not found";

var service = {};

service.createGrammar = createGrammar;
service.updateGrammar = updateGrammar;
service.deleteGrammarById = deleteGrammarById;
service.getAllGrammars = getAllGrammars;
service.getPaginationGrammars = getPaginationGrammars;
service.getGrammarsByUser = getGrammarsByUser;
service.getPaginationGrammaresByUser = getPaginationGrammarsByUser;
service.getGrammarById = getGrammarById;

module.exports = service;

Grammar = require('../models/grammar.model');

function getGrammarById(_id) {
    var deferred = Q.defer();

    try {
        Grammar.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function createGrammar(obj) {

    var deferred = Q.defer();

    try {
        // validation
        Grammar.findOne({ title: obj.title }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc) {
                deferred.reject('Grammar "' + obj.title + '" is already taken');
            } else {
                var grammar = new Grammar({});

                Object.keys(obj).forEach(function (key) {
                    grammar[key] = obj[key];
                });

                grammar.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function updateGrammar(obj) {
    var deferred = Q.defer();

    try {
        // validation
        Grammar.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc.title !== obj.title) {
                Grammar.findOne({ title: obj.title }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Grammar "' + doc.title + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            Grammar.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve(doc);
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }


    return deferred.promise;
}

function deleteGrammarById(_id) {
    var deferred = Q.defer();

    try {
        Grammar.remove({ _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getAllGrammars() {
    var deferred = Q.defer();

    try {
        Grammar.find({}).sort({ created_date: -1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationGrammars(skip, limit) {
    var deferred = Q.defer();

    try {
        Grammar.aggregate([
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getGrammarsByUser(username) {
    var deferred = Q.defer();

    try {
        Grammar.find({ created_by: username }).sort({ order: 1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationGrammarsByUser(username, skip, limit) {
    var deferred = Q.defer();

    try {
        Grammar.aggregate([
            { '$match': { 'created_by': username } },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
