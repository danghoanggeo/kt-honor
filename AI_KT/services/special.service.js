var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "Special not found";

var service = {};

service.saveOrUpdateSpecial  = saveOrUpdateSpecial ;
service.getAllSpecials = getAllSpecials;
service.getSpecialByUserAndContentId = getSpecialByUserAndContentId;
module.exports = service;

Special = require('../models/special.model');
Word = require('../models/word.model');

function getAllSpecials() {
    var deferred = Q.defer();

    try {
        Special.find({}).sort({ created_date: -1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function saveOrUpdateSpecial (obj) {
    var deferred = Q.defer();
    try {
        // validation
        Special.findOne( {$and: [{user: obj.user}, {contentId: obj.contentId}] }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                update();
            } else {
                Word.insertMany(obj.words, { ordered:false },function( err, docs){
                var special = new Special({});
                Object.keys(obj).forEach(function (key) {
                    special[key] = obj[key];
                });

                special.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            });
            }
        })

        function update() {  
            var set = {};
            set.updated_date = Date.now();
            Word.insertMany(obj.words, { ordered:false },function( err, docs){
                Object.keys(obj).forEach(function (key) {
                    set[key] = obj[key];
                });
                Special.update({$and: [{user: obj.user}, {contentId: obj.contentId}]}, { $set: _.omit(set, '_id') }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);                    
                    else deferred.resolve(doc);                
                });
            });  
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function getSpecialByUserAndContentId(user,contentId) {
    var deferred = Q.defer();

    try {
        Special.findOne({$and: [{user: user}, {contentId: contentId}] }).exec(function (err, doc) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            } else 
                deferred.resolve(doc);
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
