var _ = require('lodash');
var Q = require('q');
var mongoose = require('mongoose');
const utils = require('../helpers/utils');
const MESSAGE_NOT_FOUND = "Device not found";
var parser = require('subtitles-parser');

var service = {};

service.createDevice = createDevice;
service.updateDevice = updateDevice;
service.deleteDeviceById = deleteDeviceById;
service.getAllDevices = getAllDevices;
service.getPaginationDevices = getPaginationDevices;
service.getDeviceById = getDeviceById;
service.getDeviceByName = getDeviceByName;
service.getDeviceByDeviceId = getDeviceByDeviceId;

module.exports = service;

Device = require('../models/device.model');

function getDeviceById(_id) {
    var deferred = Q.defer();

    try {
        Device.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc[0] || doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getDeviceByDeviceId(deviceId) {
    var deferred = Q.defer();

    try {
        Device.findOne({ deviceId: deviceId } , function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getDeviceByName(name) {
    var deferred = Q.defer();    
    try {                
        Device.findOne( { name: name } , function (err, doc) {        
            if (err) {                
                deferred.resolve(null);
            } else if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve(null);
            }
        });
    } catch (ex) {
        console.log(ex);
        deferred.resolve();
    }

    return deferred.promise;
}

function createDevice(obj) {
    var deferred = Q.defer();
    try {
        // validation
        Device.findOne({ deviceId: obj.deviceId }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.reject("already taken");
            } else {
                //TODO: need to fix by me    
                var device = new Device({});
                Object.keys(obj).forEach(function (key) {
                    device[key] = obj[key];
                });
                device.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    else deferred.resolve(doc);
                });
            }
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function updateDevice(obj) {
    var deferred = Q.defer();
    try {
        // validation
        Device.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc.name !== obj.name) {
                Device.findOne({ name: obj.name }, function (err, doc_f) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc_f) {
                        deferred.reject('The device name ' + doc_f.name + ' is already taken: ');
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = {}; 
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            set.updated_date = Date.now()
            Device.updateOne({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);                    
                else deferred.resolve(doc);                
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function deleteDeviceById(_id) {
    var deferred = Q.defer();

    try {
        Device.deleteOne({ _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getAllDevices() {
    var deferred = Q.defer();

    try {
        Device.find({}).sort({ created_date: -1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationDevices(skip, limit) {
    var deferred = Q.defer();

    try {
        Device.aggregate([
            {
                $lookup: {
                    from: "devices",  // collection name in db                
                    localField: "device",
                    foreignField: "_id",
                    as: "devices"
                }
            },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
