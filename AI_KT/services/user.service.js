﻿var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var utils = require('../helpers/utils');
const config = require('../config/env.json')[process.env.NODE_ENV || 'local'];
const MESSAGE_AUTH_FAILURE = "ユーザー名かパスワードが無効";
const MESSAGE_NOT_FOUND = "User not found";

var service = {};

service.createUser = createUser;
service.changePassword = changePassword;
service.authenticateUser = authenticateUser;
service.getUserById = getUserById;
service.updateUser = updateUser;
service.deleteUserById = deleteUserById;
service.getAllUsers = getAllUsers;
service.getPaginationUsers = getPaginationUsers;
service.saveResetPasswordToken = saveResetPasswordToken;
service.getUserByResetPasswordToken = getUserByResetPasswordToken;
service.getUsersByRole = getUsersByRole;
module.exports = service;

User = require('../models/user.model');

function authenticateUser(username, password) {

    var deferred = Q.defer();
    
    try {
        User.findOne({username: username}, function(err, doc){        
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }                 
            if (doc && bcrypt.compareSync(password, doc.password)) {
                doc.token = jwt.sign({ sub: doc._id }, config.JWT.secret);                 
                deferred.resolve(doc);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }    
    
    return deferred.promise;
}

function getUserById(_id) {
    var deferred = Q.defer();

    try {
        User.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
    
            if (doc) {
                deferred.resolve(_.emit(doc, 'password'));
                //  deferred.resolve(doc);
            } else {
                // deferred.reject(MESSAGE_NOT_FOUND);
                deferred.resolve();
            }
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }    

    return deferred.promise;
}

function createUser(obj, hashPassword) {
    var deferred = Q.defer();

    try {
        // validation
    User.findOne( { username: obj.username },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
    
            if (doc) {
                // username already exists
                deferred.reject('Username "' + doc.username + '" is already taken');
            } else {
                // set user object to userParam without the cleartext password
                var user = new User({});
    
                Object.keys(obj).forEach(function(key) {
                    user[key] = obj[key];                    
                });
    
                if(hashPassword){
                    // add hashed password to user object
                    user.password = bcrypt.hashSync(obj.password, 10);
                }            
    
                user.save( function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(_.omit(doc, 'password'));
                });
            }
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }    

    return deferred.promise;
}

function changePassword(obj) {

    var deferred = Q.defer();
    
    try {
        User.findOne({ username: obj.username }, function (err, doc) {        
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc && bcrypt.compareSync(obj.oldPassword, doc.password)) {
                // authentication successful
                var set = { password : bcrypt.hashSync(obj.newPassword, 10)};
                User.update( { _id: doc._id }, { $set: set }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                        deferred.resolve(_.omit(doc, 'password'));
                    }
                );
            } else {
                // authentication failed
                deferred.reject(MESSAGE_AUTH_FAILURE);
            }
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }    

    return deferred.promise;    
}

function saveResetPasswordToken(obj) {

    var deferred = Q.defer();
    
    try {
        User.findOne({ email: obj.email }, function (err, doc) {        
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                // authentication successful
                var set = { 
                    resetPasswordToken : obj.resetPasswordToken,
                    resetPasswordExpires : obj.resetPasswordExpires
                };
                User.update( { _id: doc._id }, { $set: set }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    else deferred.resolve(_.omit(doc, 'password'));                    
                });
            } else {
                // authentication failed
                deferred.reject("No account with that email address exists.");
            }
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }    

    return deferred.promise;    
}

function getUserByResetPasswordToken(token) {
    var deferred = Q.defer();
    
    try {
        User.findOne({ resetPasswordToken: token }, function (err, doc) {        
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                if( doc.resetPasswordExpires < Date.now() )
                    deferred.reject("Your token was expired");
                else    
                    deferred.resolve(_.omit(doc, 'password'));                
            } else {
                // authentication failed
                deferred.reject("Invalid token");
            }
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }    

    return deferred.promise;
}

function updateUser(obj) {    

    var deferred = Q.defer();

    try {
        // validation
        User.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc.username !== obj.username) {
                // username has changed so check if the new username is already taken
                User.findOne( { username: obj.username }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        // username already exists
                        deferred.reject('Username "' + obj.username + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });    

        function update() {
            var set = { updated_date: Date.now() };
            
            Object.keys(obj).forEach(function(key) {
                set[key] = obj[key];        
            });                    
    
            // update password if it was entered
            if (obj.password) {
                set.password = bcrypt.hashSync(obj.password, 10);
            }   
            User.update( { _id: obj._id }, 
                { $set: _.omit(set,'password') },
                function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve(_.omit(doc, 'password'));
            });
        }

    } catch (ex) {
        deferred.reject(ex.message || ex);
    }        

    return deferred.promise;
}

function deleteUserById(_id) {
    var deferred = Q.defer();

    try {
        User.remove( { _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }    

    return deferred.promise;
}

function getAllUsers() {

    var deferred = Q.defer();
    
    try {
        User.find({}).sort({created_date:-1}).exec(function(err, docs) {     
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }                 
            if (docs) {            
                deferred.resolve(docs);
            } else {            
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }    
    
    return deferred.promise;
}

function getPaginationUsers(skip, limit) {
    
    var deferred = Q.defer();

    try {
        User.aggregate([
            { '$sort'     : { 'created_date' : -1 } },
            { '$project' : { password : 0 } },
            { '$facet'    : {
                metadatas: [ { $count: "total" } ],
                docs: [ { $skip: skip }, { $limit: limit } ]
            } }
        ] ).exec(function(err, result) {                
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }                 
            if (result) {   
                deferred.resolve(result[0] || result);
            } else {            
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }    
    
    return deferred.promise;
}

function getUsersByRole(role) {
    var deferred = Q.defer();
    
    try {
        User.find({role:role}).sort({order:1}).exec(function(err, docs) {        
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }                 
            if (docs) {         
                deferred.resolve(docs);
            } else {            
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }
    
    return deferred.promise;
}