var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "Content not found";

var service = {};

service.getContentByName = getContentByName;
service.getPaginationSearchResult = getPaginationSearchResult;

module.exports = service;

Content = require('../models/content.model');


function getContentByName(name) {
    var deferred = Q.defer();    
    try {             
        var expectedValue = name.replace(/[\u3000\u3001\u3002()\s]/g, '');
        var query = "this.name.replace(/[\u3000\u3001\u3002()\s]/g,'').indexOf('" + expectedValue + "') >= 0";
        Content.find( { $where: query } , function (err, docs) {        
            if (err) {                
                deferred.resolve(null);
            } else if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.resolve(null);
            }
        });
    } catch (ex) {
        console.log(ex);
        deferred.resolve();
    }

    return deferred.promise;
}

function getPaginationSearchResult(name, skip, limit) {
    var deferred = Q.defer();
    try {
        Content.aggregate([
            { '$match': {'name': {'$regex': name,"$options": "g"}} }, 
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
