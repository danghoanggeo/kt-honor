var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "Phrase not found";

var service = {};

service.createPhrase = createPhrase;
service.updatePhrase = updatePhrase;
service.deletePhraseById = deletePhraseById;
service.getAllPhrases = getAllPhrases;
service.getPaginationPhrases = getPaginationPhrases;
service.getPhrasesByUser = getPhrasesByUser;
service.getPaginationPhrasesByUser = getPaginationPhrasesByUser;
service.createListPhrase = createListPhrase;
service.getPhraseById = getPhraseById;
service.getPhraseByPhraseJP = getPhraseByPhraseJP;

module.exports = service;

Content = require('../models/content.model');
Phrase = require('../models/phrase.model');

function getPhraseById(_id) {
    var deferred = Q.defer();

    try {
        Phrase.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
function getPhraseByPhraseJP(value) {
    var deferred = Q.defer();
    try {
        // validation
        Phrase.findOne({ phrase_jp: value }, function (err, doc) {
            // if (err) deferred.reject(err.name + ': ' + err.message);
            if (err) {
                deferred.resolve();
            }
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function createPhrase(obj) {

    var deferred = Q.defer();

    try {
        // validation
        Phrase.findOne({ phrase_jp: obj.phrase_jp }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc) {
                deferred.reject('Phrase "' + obj.phrase_jp + '" is already taken');
            } else {
                var phrase = new Phrase({});

                Object.keys(obj).forEach(function (key) {
                    phrase[key] = obj[key];
                });

                phrase.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function createListPhrase(listObjs) {

    var deferred = Q.defer();
    try {
        Phrase.insertMany(listObjs, function (error, docs) {
            if (error) deferred.reject(error);
            else deferred.resolve(docs);
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function updatePhrase(obj) {
    var deferred = Q.defer();
    var orgDoc = {};
    try {
        // validation
        Phrase.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            orgDoc = doc;
            if (doc.phrase_jp !== obj.phrase_jp) {
                Phrase.findOne({ phrase_jp: obj.phrase_jp }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Phrase "' + doc.phrase_jp + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            Phrase.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve(doc);
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }


    return deferred.promise;
}

function deletePhraseById(_id) {
    var deferred = Q.defer();

    try {
        Phrase.remove({ _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


function getAllPhrases() {
    var deferred = Q.defer();

    try {
        Phrase.find({}).sort({ created_date: -1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationPhrases(skip, limit) {
    var deferred = Q.defer();

    try {
        Phrase.aggregate([
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPhrasesByUser(username) {
    var deferred = Q.defer();

    try {
        Phrase.find({ created_by: username }).sort({ order: 1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationPhrasesByUser(username, skip, limit) {
    var deferred = Q.defer();

    try {
        Phrase.aggregate([
            { '$match': { 'created_by': username } },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
