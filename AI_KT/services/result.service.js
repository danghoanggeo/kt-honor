var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "Result not found";

var service = {};

service.createResult = createResult;
service.updateResult = updateResult;
service.getResultById = getResultById;
service.deleteResultById = deleteResultById;
service.getAllResults = getAllResults;
service.getResultsByUser = getResultsByUser;
service.getResultByUserAndContentId = getResultByUserAndContentId;
service.submitResult = submitResult;

module.exports = service;

Result = require('../models/result.model');

function createResult(obj) {
    var deferred = Q.defer();
    try {
            
        var result = new Result({});
        Object.keys(obj).forEach(function (key) {
            result[key] = obj[key];
        });
        result.save(function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            else deferred.resolve(doc);
        });
        
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function updateResult(obj) {
    var deferred = Q.defer();
    try {
        var set = { updated_date: Date.now() };
        Object.keys(obj).forEach(function (key) {
            set[key] = obj[key];
        });
        Result.update({_id: obj._id}, { $set: _.omit(set, '_id') }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);                    
            else deferred.resolve(doc);                
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }  
    return deferred.promise;  
}

function getResultById(_id) {
    var deferred = Q.defer();
    try {
        // validation
        Result.findOne( { _id: _id }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);    
            else deferred.resolve(doc);
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    } 
    return deferred.promise; 
}

function deleteResultById(_id) {
    var deferred = Q.defer();

    try {
        Result.remove({ _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getAllResults() {
    var deferred = Q.defer();
    try {
        Result.find().exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            } else if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getResultsByUser(user) {
    var deferred = Q.defer();

    try {
        Result.find({user: user}).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            } else if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getResultByUserAndContentId(user,contentId) {
    var deferred = Q.defer();

    try {
        Result.findOne({$and: [{user: user}, {contentId: contentId}] }).exec(function (err, doc) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            } else 
                deferred.resolve(doc);
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function submitResult(obj) {
    var deferred = Q.defer();
    try {
        // validation
        Result.findOne( {$and: [{user: obj.user}, {contentId: obj.contentId}] }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                update();
            } else {
                var result = new Result({});

                Object.keys(obj).forEach(function (key) {
                    result[key] = obj[key];
                });
                result.start_date  = Date.now();
                result.end_date = Date.now();

                result.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            }
        })

        function update() {  
            var set = {};
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            set.end_date = Date.now();
             
            Result.update({$and: [{user: obj.user}, {contentId: obj.contentId}]}, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);                    
                else deferred.resolve(doc);                
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}