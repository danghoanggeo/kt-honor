var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "Kanji not found";

var service = {};

service.createKanji = createKanji;
service.updateKanji = updateKanji;
service.deleteKanjiById = deleteKanjiById;
service.getAllKanjies = getAllKanjies;
service.getPaginationKanjies = getPaginationKanjies;
service.getKanjiesByUser = getKanjiesByUser;
service.getPaginationKanjiesByUser = getPaginationKanjiesByUser;
service.createListKanji = createListKanji;
service.getKanjiById = getKanjiById;
service.getKanjiByKanjiJP = getKanjiByKanjiJP;

module.exports = service;

Content = require('../models/content.model');
Kanji = require('../models/kanji.model');

function getKanjiById(_id) {
    var deferred = Q.defer();

    try {
        Kanji.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
function getKanjiByKanjiJP(value) {
    var deferred = Q.defer();
    try {
        // validation
        Kanji.findOne({ kanji_jp: value }, function (err, doc) {
            // if (err) deferred.reject(err.name + ': ' + err.message);
            if (err) {
                deferred.resolve();
            }
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function createKanji(obj) {

    var deferred = Q.defer();

    try {
        // validation
        Kanji.findOne({ kanji_jp: obj.kanji_jp }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc) {
                deferred.reject('Kanji "' + obj.kanji_jp + '" is already taken');
            } else {
                var kanji = new Kanji({});

                Object.keys(obj).forEach(function (key) {
                    kanji[key] = obj[key];
                });

                kanji.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function createListKanji(listObjs) {

    var deferred = Q.defer();
    try {
        Kanji.insertMany(listObjs, function (error, docs) {
            if (error) deferred.reject(error);
            else deferred.resolve(docs);
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function updateKanji(obj) {
    var deferred = Q.defer();
    var orgDoc = {};
    try {
        // validation
        Kanji.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            orgDoc = doc;
            if (doc.kanji_jp !== obj.kanji_jp) {
                Kanji.findOne({ kanji_jp: obj.kanji_jp }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Kanji "' + doc.kanji_jp + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
            Object.keys(obj).forEach(function (key) {
                set[key] = obj[key];
            });
            Kanji.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve(doc);
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }


    return deferred.promise;
}

function deleteKanjiById(_id) {
    var deferred = Q.defer();

    try {
        Kanji.remove( { _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);    
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject( ex.message || ex );
    }    

    return deferred.promise;
}


function getAllKanjies() {
    var deferred = Q.defer();

    try {
        Kanji.find({}).sort({ created_date: -1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationKanjies(skip, limit) {
    var deferred = Q.defer();

    try {
        Kanji.aggregate([
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getKanjiesByUser(username) {
    var deferred = Q.defer();

    try {
        Kanji.find({ created_by: username }).sort({ order: 1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationKanjiesByUser(username, skip, limit) {
    var deferred = Q.defer();

    try {
        Kanji.aggregate([
            { '$match': { 'created_by': username } },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
