var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "class not found";

var service = {};

service.getClassById = getClassById;
service.createClass = createClass;
service.updateClass = updateClass;
service.getAllClasses = getAllClasses;
service.getPaginationClassesByTeacher = getPaginationClassesByTeacher;
service.getPaginationClasses = getPaginationClasses;
service.deleteClassById = deleteClassById;
service.getPaginationClassesByStudent = getPaginationClassesByStudent;
module.exports = service;

Class = require('../models/class.model');

function getClassById(_id) {
    var deferred = Q.defer();

    try {
        Class.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function createClass(obj) {

    var deferred = Q.defer();

    try {
        // validation
        Class.findOne({ name: obj.name }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc) {
                deferred.reject('Class "' + obj.name + '" is already taken');
            } else {
                var newclass = new Class({});

                Object.keys(obj).forEach(function (key) {
                    newclass[key] = obj[key];
                });

                newclass.save(function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function updateClass(obj) {
    var deferred = Q.defer();

    try {
        // validation
        Class.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc.name !== obj.name) {
                Class.findOne( { name: obj.name }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Class "' + doc.name + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {            
            var set = { updated_date: Date.now() };
            Object.keys(obj).forEach(function(key) {
                set[key] = obj[key];                    
            });                
            Class.update( { _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve(doc);
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    

    return deferred.promise;
}

function deleteClassById(_id) {
    var deferred = Q.defer();

    try {
        Class.find({$and:[{_id: _id }, {status: "In progress"}] }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc.length > 0) {
                deferred.reject("This class in progress. Can't delete this class");
            } else {
                Class.remove( { _id: _id }, function (err) {
                    if (err) deferred.reject(err.name + ': ' + err.message);    
                    else deferred.resolve({ id: _id });
                });
            }
        });
    } catch (ex) {
        deferred.reject( ex.message || ex );
    }    

    return deferred.promise;
}


function getAllClasses() {
    var deferred = Q.defer();

    try {
        Class.find({}).sort({ created_date: -1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationClasses(skip, limit) {
    var deferred = Q.defer();

    try {
        Class.aggregate([
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}


function getPaginationClassesByTeacher(username,skip, limit) {
    var deferred = Q.defer();

    try {
        Class.aggregate([
            { '$match':  { 'teachers':  { '$elemMatch': { 'username': username} }}}, 
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationClassesByStudent(username, skip, limit) {
    var deferred = Q.defer();

    try {
        Class.aggregate([
            { '$match':  { 'students':  { '$elemMatch': { 'username': username} }}}, 
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            } 
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}