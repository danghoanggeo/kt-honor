var _ = require('lodash');
var Q = require('q');
const MESSAGE_NOT_FOUND = "Menu not found";

var service = {};

service.createMenu = createMenu;
service.updateMenu = updateMenu;
service.deleteMenuById = deleteMenuById;
service.getAllMenus = getAllMenus;
service.getPaginationMenus = getPaginationMenus;
service.getMenusByRole = getMenusByRole;
service.getAllMenuSuper=getAllMenuSuper;

module.exports = service;

Menu = require('../models/menu.model');

function createMenu(obj) {

    var deferred = Q.defer();

    try {
        // validation
        Menu.findOne( { name: obj.name, super: obj.super }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
    
            if (doc) {
                deferred.reject('Menu "' + obj.name + '" is already taken');
            } else {
                var menu = new Menu({});
    
                Object.keys(obj).forEach(function(key) {
                    menu[key] = obj[key];                    
                });                       
    
                menu.save( function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
                    deferred.resolve(doc);
                });
            }
        });
    } catch(ex) {
        deferred.reject(ex.message || ex);
    }    

    return deferred.promise;
}

function updateMenu(obj) {
    var deferred = Q.defer();

    try {
        // validation
        Menu.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc.name !== obj.name) {
                Menu.findOne( { name: obj.name, super: obj.super }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Menu "' + doc.name + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {            
            var set = { updated_date: Date.now() };

            Object.keys(obj).forEach(function(key) {
                set[key] = obj[key];                    
            });
                
            Menu.update( { _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                deferred.resolve(doc);
            });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    

    return deferred.promise;
}

function deleteMenuById(_id) {
    var deferred = Q.defer();
    
    try {
        Menu.remove( { _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);    
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject( ex.message || ex );
    }    

    return deferred.promise;
}

function getAllMenus() {
    var deferred = Q.defer();

    try {
        Menu.find({}).sort({order:1}).exec(function(err, docs) {     
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }                 
            if (docs) {            
                deferred.resolve(docs);
            } else {            
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }    
    
    return deferred.promise;
}
function getAllMenuSuper() {
    var deferred = Q.defer();

    try {
        Menu.find({super:null}).sort({order:1}).exec(function(err, docs) {     
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }                 
            if (docs) {            
                deferred.resolve(docs);
            } else {            
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }    
    
    return deferred.promise;
}

function getPaginationMenus(skip, limit) {
    var deferred = Q.defer();

    try {
        Menu.aggregate([
            { '$sort'     : { 'order' : 1 } },
            { '$facet'    : {
                metadatas: [ { $count: "total" } ],
                docs: [ { $skip: skip }, { $limit: limit } ]
            } }
        ] ).exec(function(err, result) {                
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }                 
            if (result) {   
                deferred.resolve(result[0] || result);
            } else {            
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }    

    return deferred.promise;    
}

function getMenusByRole(role) {
    var deferred = Q.defer();

    Menu.aggregate([        
        { $match: { 'roles': role, "super":null } },
        {
            $lookup: {
                from: "menus",
                localField: "name", // Menu.name
                foreignField: "super", // menus.super
                as: "subMenus"
            }               
        },
        { $unwind: { "path": "$subMenus", "preserveNullAndEmptyArrays": true }},
        { 
            $sort: { 'subMenus.order': 1 }
        } ,
            {$match: { $or:[{'subMenus.roles': role},{$and: [{'roles': role},{"subMenus":{ $exists: false}}]}]}   
        } ,
        {
            $group: {
            _id: "$_id",
            roles: {"$first":"$roles" },
            created_date:{"$first":"$created_date"},
            updated_date:{"$first":"$updated_date"},
            name: {"$first":"$name"},
            icon: {"$first":"$icon"},
            link: {"$first":"$link"},
            super: {"$first":"$super"},
            order: {"$first":"$order"},
            __v:{"$first":"$_v"},
            subMenus: {$push: "$subMenus"}}
        },
        { 
            $sort: { order: 1 }
        } 
    ]).exec(function (err, result) {        
        if (err) {
            deferred.reject(err.name + ': ' + err.message);
        }
        if (result) {      
            deferred.resolve(result);
        } else {
            deferred.reject(MESSAGE_NOT_FOUND);
        }
    });
    
    return deferred.promise;
}