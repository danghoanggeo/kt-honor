var _ = require('lodash');
var Q = require('q');
var mongoose = require('mongoose');
const utils = require('../helpers/utils');
const MESSAGE_NOT_FOUND = "Content not found";
var parser = require('subtitles-parser');

var service = {};

service.createContent = createContent;
service.updateContent = updateContent;
service.deleteContentById = deleteContentById;
service.getAllContents = getAllContents;
service.getRootContents = getRootContents;
service.getPaginationRootContents = getPaginationRootContents;
service.getContentById = getContentById;
service.getContentByName = getContentByName;
service.getPaginationContents = getPaginationContents;
service.getContentsByUser = getContentsByUser;
service.getPaginationContentsByUser = getPaginationContentsByUser;
service.createListContent = createListContent;
service.getContentByLesson = getContentByLesson;
service.getPaginationLessonDetails = getPaginationLessonDetails;
service.getPaginationContentsByLessonByUser = getPaginationContentsByLessonByUser;

module.exports = service;

Content = require('../models/content.model');
Sentence = require('../models/sentence.model');
Word = require('../models/word.model');

function getContentById(_id) {
    var deferred = Q.defer();

    try {
        Content.findById(_id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc[0] || doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getContentByName(name) {
    var deferred = Q.defer();    
    try {                
        var expectedValue = name.replace(/[\u3000\u3001\u3002()\s]/g, '');
        var query = "this.name.replace(/[\u3000\u3001\u3002()\s]/g,'').indexOf('" + expectedValue + "') >= 0";
        console.log("query: " + query);
        Content.findOne( { $where: query } , function (err, doc) {        
            if (err) {                
                deferred.resolve(null);
            } else if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve(null);
            }
        });
    } catch (ex) {
        console.log(ex);
        deferred.resolve();
    }

    return deferred.promise;
}

function getContentByLesson(value) {
    var deferred = Q.defer();
    try {
        // validation
        Content.find({ lesson: value }, function (err, doc) {
            if (err) {
                deferred.resolve();
            }
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
function createContent(obj) {
    var deferred = Q.defer();
    try {
        // validation
        Content.findOne({ name: obj.name }, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.reject('Content "' + obj.name + '" is already taken');
            } else {
                //TODO: need to fix by me                                  
                Sentence.insertMany(obj.sentences, { ordered: false }, function (err, docs) {
                    var subtitles = parser.fromSrt(obj.subtitle);
                    subtitles.forEach(function(subtitle){
                        for(var i=0; i<obj.sentences.length; i++) {
                            if(parseInt(subtitle.id) === i + 1)
                            {
                                obj.sentences[i].startTime = utils.getTime(subtitle.startTime);
                                obj.sentences[i].endTime = utils.getTime(subtitle.endTime);
                                break;
                            }
                        }
                    })   
                    Word.insertMany(obj.words,{ordered:false},function(){
                        var content = new Content({});
                        Object.keys(obj).forEach(function (key) {
                            content[key] = obj[key];
                        });
                        content.save(function (err, doc) {
                            if (err) deferred.reject(err.name + ': ' + err.message);
                            else deferred.resolve(doc);
                        });
                    })
                });
            }
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function createListContent(listObjs) {
    var deferred = Q.defer();
    try {
        Content.insertMany(listObjs, function (error, docs) {
            if (error) deferred.reject(error);
            else deferred.resolve(docs);
        })
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function updateContent(obj) {
    var deferred = Q.defer();
    try {
        // validation
        Content.findById(obj._id, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc.name !== obj.name) {
                Content.findOne({ name: obj.name }, function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (doc) {
                        deferred.reject('Content "' + doc.name + '" is already taken')
                    } else {
                        update();
                    }
                });
            } else {
                update();
            }
        });

        function update() {
            var set = { updated_date: Date.now() };
            Sentence.insertMany(obj.sentences,{ ordered: false }, function (err, docs) {
                var subtitles = parser.fromSrt(obj.subtitle);
                subtitles.forEach(function(subtitle){
                    for(var i=0; i<obj.sentences.length; i++) {
                        if(parseInt(subtitle.id) === i + 1)
                        {
                            obj.sentences[i].startTime = utils.getTime(subtitle.startTime);
                            obj.sentences[i].endTime = utils.getTime(subtitle.endTime);
                            break;
                        }
                    }
                })    
                Word.insertMany(obj.words,{ordered:false},function(err,docsInserted){
                    // console.log(docsInserted);
                    Object.keys(obj).forEach(function (key) {
                        set[key] = obj[key];
                    });
                    Content.update({ _id: obj._id }, { $set: _.omit(set, '_id') }, function (err, doc) {
                        if (err) deferred.reject(err.name + ': ' + err.message);                    
                        else deferred.resolve(doc);                
                    });
                })
         });
        }
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }
    return deferred.promise;
}

function deleteContentById(_id) {
    var deferred = Q.defer();

    try {
        Content.remove({ _id: _id }, function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            deferred.resolve({ id: _id });
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getAllContents() {
    var deferred = Q.defer();

    try {
        Content.find({}).sort({ created_date: -1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getRootContents() {
    var deferred = Q.defer();

    Content.aggregate([
        {
            '$match': {
                $or: [
                    { 'parent': null },
                    { 'parent': "" },
                    { 'parent': { $exists: false } }]
            }
        },
        {
            $lookup: {
                from: "contents",       // collection name in db                
                localField: "name",     // Menu.name
                foreignField: "parent", // menus.super
                as: "subContents"
            }
        },
        { '$sort': { 'created_date': -1 } }
    ]).exec(function (err, result) {
        if (err) {
            deferred.reject(err.name + ': ' + err.message);
        }
        if (result) {
            deferred.resolve(result);
        } else {
            deferred.reject(MESSAGE_NOT_FOUND);
        }
    });

    return deferred.promise;
}

function getContentsByUser(username) {
    var deferred = Q.defer();

    try {
        Content.find({ created_by: username }).sort({ order: 1 }).exec(function (err, docs) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (docs) {
                deferred.resolve(docs);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationRootContents(skip, limit) {
    var deferred = Q.defer();

    try {
        Content.aggregate([
            {
                '$match': {
                    $or: [
                        { 'parent': null },
                        { 'parent': "" },
                        { 'parent': { $exists: false } }]
                }
            },

            {
                $lookup: {
                    from: "lessons",  // collection name in db                
                    localField: "lesson",
                    foreignField: "_id",
                    as: "lessons"
                }
            },
            {
                $lookup: {
                    from: "contents",       // collection name in db                
                    localField: "name",     // Menu.name
                    foreignField: "parent", // menus.super
                    as: "subContents"
                }
            },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationContents(skip, limit) {
    var deferred = Q.defer();

    try {
        Content.aggregate([
            {
                $lookup: {
                    from: "lessons",  // collection name in db                
                    localField: "lesson",
                    foreignField: "_id",
                    as: "lessons"
                }
            },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationContentsByUser(username, skip, limit) {
    var deferred = Q.defer();

    try {
        Content.aggregate([
            { '$match': { 'created_by': username } },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}

function getPaginationLessonDetails(_id, skip, limit) {
    var deferred = Q.defer();

    try {
        Content.aggregate([
            { '$match': { 'lesson': mongoose.Types.ObjectId(_id) } },
            {
                $lookup: {
                    from: "lessons",
                    localField: "lesson",
                    foreignField: "_id",
                    as: "lessons"
                }
            },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
function getPaginationContentsByLessonByUser(username, _id, skip, limit) {
    var deferred = Q.defer();

    try {
        Content.aggregate([
            { '$match': { 'created_by': username, 'lesson': mongoose.Types.ObjectId(_id) } },
            {
                $lookup: {
                    from: "lessons",
                    localField: "lesson",
                    foreignField: "_id",
                    as: "lessons"
                }
            },
            { '$sort': { 'created_date': -1 } },
            {
                '$facet': {
                    metadatas: [{ $count: "total" }],
                    docs: [{ $skip: skip }, { $limit: limit }]
                }
            }
        ]).exec(function (err, result) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            if (result) {
                deferred.resolve(result[0] || result);
            } else {
                deferred.reject(MESSAGE_NOT_FOUND);
            }
        });
    } catch (ex) {
        deferred.reject(ex.message || ex);
    }

    return deferred.promise;
}
