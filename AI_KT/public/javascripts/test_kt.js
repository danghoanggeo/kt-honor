(function() {
    'use strict';
  
    // ボタン系
    //var btnConnect = document.querySelector('.btn-connect');
    // var btnDisconnect = document.querySelector('.btn-disconnect');
    var btnPublishStart = document.querySelector('.btn-publish-start');
    var selectLesson = document.querySelector('.input-lesson');
    // var btnUnsubscribe = document.querySelector('.btn-unsubscribe');
    // var btnClear = document.querySelector('.btn-clear');
  
    // 入力系
    //var inputTopicPub = document.querySelector('.input-topic-pub');
    var inputMessage = document.querySelector('.input-message');
    var inputUserQuz = document.querySelector('.input-user-quz');
    var inputLesson = document.querySelector('.input-lesson');
  
    var messages = document.querySelector('.my-messages');
    var lessons = document.querySelector('.class-korean-lesson');

    var client, appendMessage, clearMessages, clearLessons;
    var TOPIC_HONOR_CONVER_START_OK = "honor/topic-con-start-ok";
    var SUB_TOPIC_USER_SPEAK_OK = "honor/topic-user-speak-ok"
    var TOPIC_HONOR_CONVER_START = "honor/topic-con-start"
    //e.preventDefault();
    client = mows.createClient("ws://test.mosquitto.org:8080/mqtt");
    console.log('connection open :)');
    client.on('message', function (topic, message) {
        console.log(message);
        appendMessage(message);
    });
    client && client.subscribe(SUB_TOPIC_USER_SPEAK_OK);
    client && client.subscribe(TOPIC_HONOR_CONVER_START_OK);
    console.log('subscribe -> demo/mqtt-hack-ai');
  
    // btnDisconnect.addEventListener('click', function(e) {
    //   e.preventDefault();
    //   client && client.end();
    //   appendMessage('connection closed');
    // });
    
    btnPublishStart.addEventListener('click',function(e){
        e.preventDefault();
        clearMessages();
      var lesson = inputLesson.value;
      var user_quz = inputUserQuz.value;
      var sendData = JSON.stringify({"uid":user_quz,"lesson":lesson,"sen_num":"1"});
      console.log(sendData);
      client && client.publish(TOPIC_HONOR_CONVER_START,sendData);
    });


    selectLesson.addEventListener('change', function(e) {
      e.preventDefault();
      clearLessons();
      var lesson = inputLesson.value;
      var text_div = $('<div class = "row"></div>');
      if(lesson == "con1"){
        text_div.append('<div class="row"><div class="col-lg-3"><i class="fa fa-user text-aqua"></i>&nbsp first :</div><div class="col-lg-6"><h4>안녕하세요</h4></div><div class="col-lg-3"><h5>annyeonghaseyo</h5></div></div><div class="row"><div class="col-lg-3"><i class="fa fa-user"></i>&nbsp second :</div><div class="col-lg-6"><h4>안녕하세요. 저는 타완이에요.</h4></div><div class="col-lg-3"><h5>annyeonghaseyo. jeoneun tawan-ieyo.</h5></div></div><div class="row"><div class="col-lg-3"><i class="fa fa-user text-aqua"></i>&nbsp first :</div><div class="col-lg-6"><h4>의사예요?</h4></div><div class="col-lg-3"><h5>uisayeyo?</h5></div></div><div class="row"><div class="col-lg-3"><i class="fa fa-user"></i>&nbsp second :</div><div class="col-lg-6"><h4>네, 의사예요.</h4></div><div class="col-lg-3"><h5>ne, uisayeyo.</h5></div></div>');
        $(".lesson-title").text("안녕하세요");
    }
      if(lesson == "con2"){
        text_div.append('<div class="row"><div class="col-lg-3"><i class="fa fa-user text-aqua"></i>&nbsp first :</div><div class="col-lg-6"><h4>수진씨는 뭐해요?</h4></div><div class="col-lg-3"><h5>sujinssineun mwohaeyo?</h5></div></div><div class="row"><div class="col-lg-3"><i class="fa fa-user"></i>&nbsp second :</div><div class="col-lg-6"><h4>저는 공부해요.</h4></div><div class="col-lg-3"><h5>jeoneun gongbuhaeyo.</h5></div></div><div class="row"><div class="col-lg-3"><i class="fa fa-user text-aqua"></i>&nbsp frist :</div><div class="col-lg-6"><h4>운동해요. 마크씨는 뭐해요?</h4></div><div class="col-lg-3"><h5>undonghaeyo. makeussineun mwohaeyo?</h5></div></div><div class="row"><div class="col-lg-3"><i class="fa fa-user"></i>&nbsp second :</div><div class="col-lg-6"><h4>너는 너무 열심히 일해.</h4></div><div class="col-lg-3"><h5>neoneun neomu yeolsimhi ilhae.</h5></div></div>');
        $(".lesson-title").text("수진씨는 뭐해요?");
    }
      text_div.appendTo(lessons);
    });
  
    // btnUnsubscribe.addEventListener('click', function(e) {
    //   e.preventDefault();
    //   client && client.unsubscribe(inputTopicSub.value);
    //   appendMessage('unsubscribe -> ' + inputTopicSub.value);
    // });
  
    // btnClear.addEventListener('click', function(e) {
    //   e.preventDefault();
    //   clearMessages();
    // });
    
  
    appendMessage = function(message) {
      var text_val = $('<div class = "row"></div>');
      message = JSON.parse(message);
      console.log(message);
      if(message.uid == "0"){
            if(message.type =="say"){ // please say
                text_val.append('<div class="direct-chat-msg"><div class="direct-chat-primary clearfix"><span class="direct-chat-name pull-left" style="padding-left:20px;">AI-KT</span></div><img class="direct-chat-img" src="/images/asitan.png" alt="Message User Image"><div class="direct-chat-text" style="background: #00c0ef;border-color: #00c0ef;color: #ffffff;">'+message.content+'</div></div>');
            }else if(message.type == "sorry"){ //When say not correct 5 times. will out of program
                text_val.append('<div class="direct-chat-msg"><div class="direct-chat-primary clearfix"><span class="direct-chat-name pull-left" style="padding-left:20px;">AI-KT</span></div><img class="direct-chat-img" src="/images/asitan.png" alt="Message User Image"><div class="direct-chat-text" style="background: #dd4b39;border-color: #dd4b39; color: #ffffff;">'+message.content+'</div></div>');
            }else if(message.type == "repeat"){ // User say not correct, so please repeat
                text_val.append('<div class="direct-chat-msg"><div class="direct-chat-primary clearfix"><span class="direct-chat-name pull-left" style="padding-left:20px;">AI-KT</span></div><img class="direct-chat-img" src="/images/asitan.png" alt="Message User Image"><div class="direct-chat-text" style="background: #f39c12;border-color: #f39c12;color: #ffffff;">'+message.content+'</div></div>');
            }
            else{// thanks for finished
                text_val.append('<div class="direct-chat-msg"><div class="direct-chat-primary clearfix"><span class="direct-chat-name pull-left" style="padding-left:20px;">AI-KT</span></div><img class="direct-chat-img" src="/images/asitan.png" alt="Message User Image"><div class="direct-chat-text" style="background: #00c0ef;border-color: #00c0ef;color: #ffffff;">'+message.content+'</div></div>');
            }
        }
        else
        {
            if(message.speak == "0"){
                text_val.append('<div class="direct-chat-msg right"><div class="direct-chat-default clearfix"><span class="direct-chat-name pull-right" style="padding-right:20px;">You</span></div><img class="direct-chat-img" src="/admin-lte/dist/img/user2-160x160.jpg" alt="Message User Image"><div class="direct-chat-text" style="background: #666666;border-color:  #666666; color: #ffffff;">'+message.content+'</div></div>');
            }else{
                text_val.append('<div class="direct-chat-msg right"><div class="direct-chat-default clearfix"><span class="direct-chat-name pull-right" style="padding-right:20px;">You</span></div><img class="direct-chat-img" src="/admin-lte/dist/img/user2-160x160.jpg" alt="Message User Image"><div class="direct-chat-text" style="background: #269cd3;border-color: #269cd3; color: #ffffff;">'+message.content+'</div></div>');
            }
            
        }
      
      //var string_ = document.createTextNode(text_val);
      text_val.appendTo(messages);
      document.getElementById('scroll-bottom').offsetTop;
      document.getElementById('scroll-bottom').scrollIntoView();
      client && client.publish("mqtt/demo-kt-confirm",JSON.stringify({"status":1,"lesson":message.lesson,"sen_num":message.sen_num,"uid":message.uid}));
    }
    
    clearLessons = function(){
        var count = lessons.childNodes.length;
        for(var i=0; i<count; i++) {
            lessons.removeChild(lessons.firstChild);
        }
    }

    clearMessages = function() {
      var count = messages.childNodes.length;
      for(var i=0; i<count; i++) {
        messages.removeChild(messages.firstChild);
      }
    }
  
  })();