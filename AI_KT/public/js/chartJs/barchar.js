
var myBarChart = new Chart(ctx).Bar(data, options);
var data = {
	labels: ["January", "February", "March", "April", "May", "June", "July"],
	datasets: [
		{
			label: "My First dataset",
			fillColor: "rgba(220,220,220,0.5)",
			strokeColor: "rgba(220,220,220,0.8)",
			highlightFill: "rgba(220,220,220,0.75)",
			highlightStroke: "rgba(220,220,220,1)",
			data: [65, 59, 80, 81, 56, 55, 40]
		},
		{
			label: "My Second dataset",
			fillColor: "rgba(151,187,205,0.5)",
			strokeColor: "rgba(151,187,205,0.8)",
			highlightFill: "rgba(151,187,205,0.75)",
			highlightStroke: "rgba(151,187,205,1)",
			data: [28, 48, 40, 19, 86, 27, 90]
		}
	]
};

{
	//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	scaleBeginAtZero : true;

	//Boolean - Whether grid lines are shown across the chart
	scaleShowGridLines : true;

	//String - Colour of the grid lines
	scaleGridLineColor : "rgba(0,0,0,.05)";

	//Number - Width of the grid lines
	scaleGridLineWidth : 1;

	//Boolean - Whether to show horizontal lines (except X axis)
	scaleShowHorizontalLines: true;

	//Boolean - Whether to show vertical lines (except Y axis)
	scaleShowVerticalLines: true;

	//Boolean - If there is a stroke on each bar
	barShowStroke : true;

	//Number - Pixel width of the bar stroke
	barStrokeWidth : 2;

	//Number - Spacing between each of the X value sets
	barValueSpacing : 5;

	//Number - Spacing between data sets within X values
	barDatasetSpacing : 1;
	{ raw }
	//String - A legend template
	legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
	{endraw }
}

new Chart(ctx).Bar(data, {
	barShowStroke: false
});
// This will create a chart with all of the default options, merged from the global config,
//  and the Bar chart defaults but this particular instance will have `barShowStroke` set to false.

canvas.onclick = function(evt){
	var activeBars = myBarChart.getBarsAtEvent(evt);
	// => activeBars is an array of bars on the canvas that are at the same position as the click event.
};

myBarChart.datasets[0].bars[2].value = 50;
// Would update the first dataset's value of 'March' to be 50
myBarChart.update();
// Calling update now animates the position of March from 90 to 50.


//Calling `addData(valuesArray, label)` on your Chart instance passing an array of values for each dataset, along with a label for those bars.


// The values array passed into addData should be one for each dataset in the chart
//myBarChart.addData([40, 60], "August");
// The new data will now animate at the end of the chart.


//removeData( )

//Calling `removeData()` on your Chart instance will remove the first value for all datasets on the chart.

```javascript
myBarChart.removeData();
// The chart will now animate and remove the first bar
```
