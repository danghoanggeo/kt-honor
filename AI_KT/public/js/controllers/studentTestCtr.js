// public/sentences.js
var studentTestCtr = angular.module('studentTestCtr', []);


studentTestCtr.controller('testHiraganaCtr',function testHiraganaCtr($scope, $http,$timeout){
    $scope.formHiragana =  {};
    //$scope.formHiragana.answer =  "1";
    $scope.testTitle = "ひらがなを一つ書いて";
    var levelId = "N5";
    // Countdown
    $scope.countDown = 90;    
    
    $scope.onTimeout = function(){
        $scope.countDown--;
        mytimeout = $timeout($scope.onTimeout,1000);
        if($scope.countDown == 0){
            $timeout.cancel(mytimeout);
        }
    }
    var mytimeout = $timeout($scope.onTimeout,1000);

    // Get all sentences
    $http({method: 'GET', url: '/gakusei/test/hiragana/'+ levelId}).
        then(function(response) {
            //$scope.status = response.status;
            $scope.quizs = response.data;
            console.log(response.data);
        }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            //$scope.status = response.status;
            console.log(response.data);
    });

    $scope.submitTestAnswer = function() {
        //$scope.formHiragana.lesson = "aaaaa";
        console.log($scope.formHiragana);
        var answer = $scope.formHiragana;
        $http({method: 'POST', url: '/gakusei/test/hiragana/submit',data:answer}).
          then(function(response) {
            $scope.status = response.status;
            $scope.answerResults = response.data;
            console.log(response.data);
          }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            $scope.status = response.status;
        });
        
    };

    $scope.range = function(max) {
        var min = 1;
        var step = 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    };
    
    
});

studentTestCtr.controller('testGimonshiCtr',function testGimonshiCtr($scope, $http,$timeout){
    $scope.formGimonshi =  {};
    //$scope.formHiragana.answer =  "1";
    $scope.testTitle = "[＿＿]の中に適当な疑問詞を書いてください。";
    var levelId = "N5";
    // Countdown
    $scope.countDown = 90;    
    
    $scope.onTimeout = function(){
        $scope.countDown--;
        mytimeout = $timeout($scope.onTimeout,1000);
        if($scope.countDown == 0){
            $timeout.cancel(mytimeout);
        }
    }
    var mytimeout = $timeout($scope.onTimeout,1000);

    // Get all sentences
    $http({method: 'GET', url: '/gakusei/test/gimonshi/'+ levelId}).
        then(function(response) {
            //$scope.status = response.status;
            $scope.quizs = response.data;
            console.log(response.data);
        }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            $scope.status = response.status;
            console.log(response.data);
    });

    $scope.submitTestAnswer = function() {
        //$scope.formHiragana.lesson = "aaaaa";
        console.log($scope.formGimonshi);
        var answer = $scope.formGimonshi;
        $http({method: 'POST', url: '/gakusei/test/gimonshi/submit',data:answer}).
          then(function(response) {
            $scope.status = response.status;
            $scope.answerResults = response.data;
            console.log(response.data);
          }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            $scope.status = response.status;
        });
        
    };

    $scope.range = function(max) {
        var min = 1;
        var step = 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    };
    
    
});