// public/sentences.js
var teacherTestCtr = angular.module('teacherTestCtr', []);


teacherTestCtr.controller('testHiraganaCtr',function testHiraganaCtr($scope, $http){
    $scope.formCreateHiraganaData = {};
    
    $scope.count = 1;
    $scope.testTitle = "ひらがなを一つ書いて";
    var levelId = "N5";
    // Get all sentences
    $http({method: 'GET', url: '/teacher/test/hiragana/'+ levelId}).
        then(function(response) {
            $scope.status = response.status;
            $scope.quizs = response.data;
            console.log(response.data);
        }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            $scope.status = response.status;
            console.log(response.data);
    });

    $scope.createTestHiragana = function() {
        $scope.code = null;
        $scope.response = null;
        var answers = $scope.formHiragana.answer;
        console.log(answers);
        objTestHiragana= {
            quiz : $scope.formHiragana.quiz,
            answer : answers.split(" "),
            explain: $scope.formHiragana.explain,
            level: $scope.formHiragana.level,
            lesson: $scope.formHiragana.lesson
        };
        
        $http({method: 'POST', url: '/teacher/test/hiragana/',data:objTestHiragana}).
          then(function(response) {
            $scope.status = response.status;
            $scope.quizs = response.data;
          }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            $scope.status = response.status;
        });
    };
  
    $scope.deleteTestHiragana = function(testId){
        console.log(testId);
        datareq = {test:testId,
                level:"N5"};
        $http({method: 'POST', url: '/teacher/test/hiragana/delete',data:datareq}).
          then(function(response) {
            $scope.status = response.status;
            $scope.quizs = response.data;
            console.log(response.data);
          }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            console.log(response.data);
            $scope.status = response.status;
        });
    };
});

teacherTestCtr.controller('testGimonshi',function testGimonshi($scope, $http){
    $scope.testTitle = "[＿＿]の中に適当な疑問詞を書いてください。";
    var levelId = "N5";
    // Get all sentences
    $http({method: 'GET', url: '/teacher/test/gimonshi/'+ levelId}).
        then(function(response) {
            $scope.status = response.status;
            $scope.quizs = response.data;
            console.log(response.data);
        }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            $scope.status = response.status;
            console.log(response.data);
    });

    $scope.createTestGhimonshi = function() {
        $scope.code = null;
        $scope.response = null;
        var answers = $scope.formGhimonshi.answer;
        console.log(answers);
        objTestGhimonshi= {
            quiz : $scope.formGhimonshi.quiz,
            answer : answers.split(" "),
            explain: $scope.formGhimonshi.explain,
            level: $scope.formGhimonshi.level,
            lesson: $scope.formGhimonshi.lesson
        };
        
        $http({method: 'POST', url: '/teacher/test/gimonshi/',data:objTestGhimonshi}).
          then(function(response) {
            $scope.status = response.status;
            $scope.quizs = response.data;
          }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            $scope.status = response.status;
        });
    };
  
    $scope.deleteTestGhimonshi = function(testId){
        console.log(testId);
        datareq = {test:testId,
                level:"N5"};
        $http({method: 'POST', url: '/teacher/test/gimonshi/delete',data:datareq}).
          then(function(response) {
            $scope.status = response.status;
            $scope.quizs = response.data;
            console.log(response.data);
          }, function(response) {
            $scope.quizs = response.data || 'Request failed';
            console.log(response.data);
            $scope.status = response.status;
        });
    };
});