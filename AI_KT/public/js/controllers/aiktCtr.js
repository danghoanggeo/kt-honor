
// public/sentences.js
var converApp = angular.module('converApp', ['pubnub.angular.service']);

converApp.controller('converCtr',function converCtr($scope, $http ,$timeout,Pubnub){
  
  $scope.channel = 'messages-channel';
   // Generating a random uuid between 1 and 100 using an utility function from the lodash library.         
      $scope.uuid = 1;
      var defaultInstance = new PubNub({
          publishKey: 'pub-c-e168f0fc-9daf-4d0b-87ec-6f4da18d9b38',
          subscribeKey: 'sub-c-6d4aa30c-b33c-11e8-80bd-3226ad0d6938'
        });
      
       console.log("Init successful!");
      // Send the messages over PubNub Network
      $scope.sendMessage = function() {
        // Don't send an empty message 
        console.log($scope.messageContent);
        if (!$scope.messageContent || $scope.messageContent === '') {
             return;
         }
         defaultInstance.publish({
             channel: $scope.channel,
             message: {
                 content: $scope.messageContent,
                 sender_uuid: $scope.uuid,
                 date: new Date()
             },
             callback: function(m) {
                 console.log(m);
             }
         });
         // Reset the messageContent input
         $scope.messageContent = '';
 
     }
     $scope.messages = [];

    // Subscribing to the ‘messages-channel’ and trigering the message callback
    defaultInstance.subscribe({
        channel: $scope.channel,
        triggerEvents: ['callback']
    });

    // Listening to the callbacks
    $scope.$on(PubNub.getMessageEventNameFor($scope.channel), function (ngEvent, m) {
        $scope.$apply(function () {
            $scope.messages.push(m)
        });
    });

  // Countdown
  $scope.countDown = 90;    
  
  $scope.onTimeout = function(){
      $scope.countDown--;
      mytimeout = $timeout($scope.onTimeout,1000);
      if($scope.countDown == 0){
          $timeout.cancel(mytimeout);
      }
  }
  var mytimeout = $timeout($scope.onTimeout,1000);

  // Get all sentences
  $http({method: 'GET', url: '/gakusei/test/hiragana/'+ levelId}).
      then(function(response) {
          //$scope.status = response.status;
          $scope.quizs = response.data;
          console.log(response.data);
      }, function(response) {
          $scope.quizs = response.data || 'Request failed';
          //$scope.status = response.status;
          console.log(response.data);
  });


  $scope.range = function(max) {
      var min = 1;
      var step = 1;
      var input = [];
      for (var i = min; i <= max; i += step) {
          input.push(i);
      }
      return input;
  };
  
  
});
