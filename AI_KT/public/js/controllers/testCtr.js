var converApp = angular.module('converApp', ['pubnub.angular.service']);
  
converApp.run(['Pubnub', function (Pubnub) {
    var randomAuthKey = PubNub.generateUUID();
    Pubnub.init({
        publishKey : 'pub-c-b77ad9bc-d3b6-4f55-a39b-4516c6bbc4ab',
        subscribeKey : 'sub-c-10d05e36-b33d-11e8-a6d9-8605c8522785'
    });
    console.log(randomAuthKey);
}]);
  
converApp.controller('converCtr', ['$scope','$rootScope','Pubnub', function($scope,$rootScope,Pubnub) {
    $scope.channel = 'messages-channel';
    $scope.uuid = 1;
    Pubnub.subscribe({
        channel: 'my_channel',
        message: function(m){console.log(m);},
        connect: Pubnub.publish({
            channel: 'my_channel',
            message: 'Hello from the PubNub Javascript SDK'
        })
    });
    Pubnub.subscribe({
        channel: $scope.channel,
        triggerEvents: ['message']
    });
    $scope.sendMessage = function() {
        // Don't send an empty message 
        console.log($scope.messageContent);
        if (!$scope.messageContent || $scope.messageContent === '') {
             return;
         }
         Pubnub.publish({
            channel: $scope.channel,
            message: {
                content: $scope.messageContent,
                sender_uuid: $scope.uuid
            },
            triggerEvents: ['callback']
        });
         // Reset the messageContent input
         $scope.messageContent = '';
 
     }
     $scope.messages = [];

    // Listening to the callbacks
    $rootScope.$on(Pubnub.getMessageEventNameFor($scope.channel), function (ngEvent, envelope) {
        $scope.$apply(function () {
            console.log("message");
            $scope.messages.push(envelope);
        });
    });
    $rootScope.$on(Pubnub.getEventNameFor('publish', 'callback'), function (ngEvent, status, response) {
        $scope.$apply(function () {
            if (status.error){
               $scope.statusSentSuccessfully = false;
               console.log('0');
            } else {
               $scope.statusSentSuccessfully = true;
               console.log('1');
            }
         })
    });
}]);
  
converApp.controller('WorldCtrl', function($scope, $rootScope, Pubnub) {
    $rootScope.$on(Pubnub.getMessageEventNameFor($scope.selectedChannel), function (ngEvent, envelope) {
        $scope.$apply(function () {
            // add message to the messages list
            $scope.chatMessages.unshift(envelope.message);
        });
    });
  
    $rootScope.$on(Pubnub.getEventNameFor('publish', 'callback'), function (ngEvent, status, response) {
        $scope.$apply(function () {
            if (status.error){
               $scope.statusSentSuccessfully = false;
            } else {
               $scope.statusSentSuccessfully = true;
            }
         })
    });
});