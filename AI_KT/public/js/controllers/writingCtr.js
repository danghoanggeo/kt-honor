// public/sentences.js
var scotchWriting = angular.module('scotchWriting', []);

scotchWriting.controller('writingController',function testGimonshi($scope, $http){
    $scope.formData = {};
    
    $scope.count = 1;
    $scope.showanswer = false;
    // Get all sentences
    $http({method: 'GET', url: '/gakusei/sentences'}).
        then(function(response) {
            $scope.status = response.status;
            $scope.sentences = response.data;
        }, function(response) {
            $scope.sentences = response.data || 'Request failed';
            $scope.status = response.status;
    });
    // when submitting the add form, send the text to the node API
    $scope.createWriting = function() {
        $http({method: 'POST',url:'/student/writings',data: $scope.formData})
            .then(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.writings = data;
                //console.log(data);
            },function(response) {
                $scope.sentences = response.data || 'Request failed';
                $scope.status = response.status;
        });
    };

    $scope.getAnalyzeSentence = function(){
        // Data cannot be null
        if($scope.formData == ""){
            //TODO: inform to user analyzed data cannot be null
        }else{
            //console.log($scope.formData);
            sentences = $scope.formData.text;
            var index = $scope.count;
            
            $http({method: 'POST',url:'/student/sentence/analyze',data:{"index":index,"sentence":sentences}})
            .then(function(data){
                $scope.formData = {};
                var flag = false;
                var result = "";
                for(word in data){
                    if(!data[word]["valid"] && !flag){
                        result += "<strike>"+data[word]["word"];
                        flag = true;
                    }
                    else if(data[word]["valid"] && flag==true){
                        result += "</strike>"+data[word]["word"];
                        flag = false;
                    }else{
                        result += data[word]["word"];
                    }
                }
                $scope.words = result;
                $scope.showanswer = true;
            },function(data){
                console.log('Error-Alanyze sentence: '+data);
            });
        }
    }
    // update a writing after checking it
    $scope.updateWriting = function(id) {
        $http.post('/student/writings/' + id)
            .success(function(data) {
                $scope.writings = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

    // delete a writing after checking it
    $scope.deleteWriting = function(id) {
        $http({method: 'POST',url:'/student/writings/' + id})
            .then(function(data) {
                $scope.writings = data;
                console.log(data);
            },function(data) {
                console.log('Error: ' + data);
            });
    };

    $scope.range = function(max) {
        var min = 1;
        var step = 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    };
    

});