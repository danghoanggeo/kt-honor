const express = require('express');
const router = express.Router();
const utils = require('../helpers/utils');

// Services
const testHiraganaService = require('../services/test/test.hiraga.service');
const testGimonshiSevice = require('../services/test/test.gimonshi.service');


// Student home page
router.get('/', function(req, res,next) {
    utils.render(req, res, 'pages/student/student', { title: 'Student | Home', menus: [], error: "" });
});

// student gakushuy
router.get('/learn',function(req,res,next){
    utils.render(req,res,'pages/student/gakushuy',{title:'gakusei | gakushuy',menus: [], error: ""});
});

// student renshuy
router.get('/renshuy',function(req,res,next){
    utils.render(req,res,'pages/student/renshuy',{title:'gakusei | gakushuy',menus: [], error: ""});
});

// student test
router.get('/test',function(req,res,next){
    utils.render(req,res,'pages/student/test',{title:'gakusei | gakushuy',menus: [], error: ""});
});

// student schedule
router.get('/schedule',function(req,res,next){
    utils.render(req,res,'pages/student/schedule',{title:'gakusei | gakushuy',menus: [], error: ""});
});

// Student profile
router.get('/profile/:studentId',function(req,res,next){
    utils.render(req,res,'pages/student/profile',{title:'gakusei | profile',menu:[],error:""});
});
// Student profile
router.get('/index',function(req,res,next){
    utils.render(req,res,'pages/student/index',{title:'gakusei | profile',menu:[],error:""});
});

//API:

//  get all testHiraganaHitotsu
router.get('/test/hiragana/:levelId', function(req, res,next) {
    try{
        var levelId  = req.params.levelId;
        testHiraganaService.getAllTestHiraga(levelId).then(function (hiraganas) {
			var result = {};
			var index = 1;
			for(hiragana in hiraganas){
				var id = hiraganas[hiragana]["_id"];
				result[id] =  {_id:id,index:index,quiz:hiraganas[hiragana]["quiz"],numAnswer:hiraganas[hiragana]["answer"].length};
				index ++;
			}
			//console.log(result);
            res.json(result);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});

//submit testHiraganaHitotsu answers
router.post('/test/hiragana/submit', function(req, res,next) {
    try{
        
        var answers  = req.body.answers;
        //console.log(answers);
        testHiraganaService.checkAnswerById(answers).then(function (result) {
            res.json(result);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});

//  get all gimonshi
router.get('/test/gimonshi/:levelId', function(req, res,next) {
    try{
        var levelId  = req.params.levelId;
        testGimonshiSevice.getAlltestGimonshi(levelId).then(function (gimonshis) {
			var result = {};
			var index = 1;
			for(gimonshi in gimonshis){
				var id = gimonshis[gimonshi]["_id"];
				result[id] =  {_id:id,index:index,quiz:gimonshis[gimonshi]["quiz"]};
				index ++;
			}
			//console.log(result);
            res.json(result);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});

//submit gimonshi answers
router.post('/test/gimonshi/submit', function(req, res,next) {
    try{
        
        var answers  = req.body.answers;
        //console.log(answers);
        testGimonshiSevice.checkAnswerById(answers).then(function (result) {
            res.json(result);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});

module.exports = router