const express = require('express')
const router = express.Router()
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
const classServices = require('../services/class.service');
const menuServices = require('../services/menu.service');
const utils = require('../helpers/utils');

router.get('/detail/:id', ensureLoggedIn('/login'), function (req, res) {
	res.cookie('classId', req.params.id);
	menuServices.getMenusByRole(req.session.passport.user.role).then(function (menus) {
		utils.render(req, res, 'pages/students', { title: 'Class Detail Page', menus: menus });
	}).catch(function (error) {
		utils.render(req, res, 'pages/errors', { title: 'Error Page', error: { message: error.message || error } });
	});	
});

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {			
	classServices.createClass(req.body).then(function(result){
		res.json({class:result});
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {	
	classServices.updateClass(req.body).then(function(result){
		res.json({class:result});
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {		
	classServices.deleteClassById(req.params.id).then(function(result){		
		res.json(result);
		
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.get('/all', function(req, res, next){
	classServices.getAllClasses().then(function(classes){
		res.json({classes:classes});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/paginate/:username/:skip/:limit', function(req, res, next){
	const username = req.params.username;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	classServices.getPaginationClassesByTeacher(username,skip, limit).then(function(result){		
		res.json({classes:result.docs, total: utils.getPaginateTotalResult(result.metadatas)});		
		res.json({error: error.message || error});
	});
});

router.get('/:id', function(req, res, next){
	classServices.getClassById(req.params.id).then(function(result){
		res.json({class:result});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/paginate/:skip/:limit', function(req, res, next){
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	classServices.getPaginationClasses(skip, limit).then(function(result){		
		res.json({classes:result.docs, total: utils.getPaginateTotalResult(result.metadatas)});
	}).catch(function(error){		
		res.json({error: error.message || error});
	});
});

router.get('/lesson/:username/:skip/:limit', function(req, res, next){
	const username = req.params.username;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	classServices.getPaginationClassesByStudent(username, skip, limit).then(function(result){
		res.json({classes:result.docs,total: utils.getPaginateTotalResult(result.metadatas)});
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

module.exports = router