const express = require('express')
const router = express.Router()
const passport = require('../helpers/passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn
const userServices = require('../services/user.service');
const MESSAGE_AUTH_FAILURE = "ユーザー名かパスワードが無効. もう一度試してください。";
const fs = require('fs');
const multer = require('multer');
const utils = require('../helpers/utils');

var storage = multer.diskStorage({ //multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './public/uploads/Image');
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});

var upload = multer({ //multer settings
	storage: storage
}).single('file');

router.post('/authenticate', function (req, res, next) {
	passport.authenticate('local', function (err, user) {
		if (err) {
			res.json({ error: err });
			return;
		}

		if (!user) {
			res.json({ error: MESSAGE_AUTH_FAILURE });
			return;
		}

		var promise = Promise.resolve();
		promise.then(() => {
			req.logIn(user, function (err) {
				if (!req.body.rememberMe)
					delete req.session.cookie.originalMaxAge;
				if (err)
					res.json({ error: err });
				// res.redirect( req.session.returnTo || "/");																	
				else
					res.json({
						returnTo: req.session.returnTo || "/",
						user: user
					});
			})
		}).catch(err => {
			res.json({ error: err });
		})

	})(req, res, next);
});


router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {
	userServices.createUser(req.body, true).then(function (user) {
		res.json({ user: user });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.post('/changePassword', ensureLoggedIn('/login'), function (req, res, next) {
	userServices.changePassword(req.body).then(function (user) {
		res.json({ user: user });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {
	userServices.updateUser(req.body).then(function (user) {
		res.json({ user: user });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {
	userServices.deleteUserById(req.params.id).then(function (result) {
		res.json(result);
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/all', function (req, res, next) {
	userServices.getAllUsers().then(function (users) {
		res.json({ users: users });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/total', function (req, res, next) {
	userServices.getTotalRows().then(function (result) {
		res.json(result);
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	userServices.getPaginationUsers(skip, limit).then(function (result) {
		res.json({ users: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/:id', function (req, res, next) {
	userServices.getUserById(req.params.id).then(function (user) {
		res.json({ user: user });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/role/:role', function (req, res, next) {
	userServices.getUsersByRole(req.params.role).then(function (users) {
		res.json({ users: users });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.post('/upload/avatar', ensureLoggedIn('/login'), function (req, res) {

	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
			return;
		}
		if (req.file) {
			try {
				var selectedUser = req.body.selectedUser;
				selectedUser.avatar = req.file.originalname;
				userServices.updateUser(selectedUser).then(function (result) {
					res.json({ user: selectedUser });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: ex.message || ex });
			}
		} else {
			userServices.updateUser(req.body).then(function (result) {
				res.json({ user: req.body });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});
module.exports = router