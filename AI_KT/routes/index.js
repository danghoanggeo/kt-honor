var _ = require('lodash');
const express = require('express')
//const pubnub = require('pubnub')
const router = express.Router()
const utils = require('../helpers/utils');
const userServices = require('../services/user.service');

// Create defalut admin user
userServices.createUser({
	fullname: 'admin',
	username: 'admin',
	password: '$2a$10$Ra6RDnwChiPhzwEx9Cwh1e2E1BM3BJUQszwSwYHqMI4eDTbdWCeq6',
	email: 'admin@gmail.com',
	role: 'admin'
}, false).then(function (user) {
	console.log('Created default user');
}).catch(function (err) {
	console.log(err);
});

router.use('/menu', require('./menu'))
router.use('/user', require('./user'))
router.use('/content', require('./content'))
router.use('/course', require('./course'))
router.use('/sentence', require('./sentence'));
router.use('/word', require('./word'));
router.use('/phrase', require('./phrase'));
router.use('/lesson', require('./lesson'));
router.use('/grammar', require('./grammar'));
router.use('/quiz', require('./quiz'));
router.use('/result', require('./result'));
router.use('/class', require('./class'));
router.use('/kanji', require('./kanji'));
router.use('/search', require('./search'));
router.use('/special', require('./special'));
// ---------------------
router.use('/student',require('./student'));
router.use('/gakusei', require('./writings'));
router.use('/teacher',require('./teacher'));





router.get('/', function (req, res, next) {
	utils.render(req, res, 'pages/student/gakushuy', { title: 'Home page', menus: [], error: "" });
});

router.get('/community',function(req,res,next){
	utils.render(req,res,'pages/student/community',{title: 'Community',error:""});
});
 
router.get('/test', function (req, res, next) {
	utils.render(req, res, 'pages/writing', { title: 'Home page', menus: [], error: "" });
});

module.exports = router