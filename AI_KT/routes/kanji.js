const express = require('express')
const router = express.Router()
const passport = require('../helpers/passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
const kanjiServices = require('../services/kanji.service');
const multer = require('multer');
const utils = require('../helpers/utils');

var storage = multer.diskStorage({ //multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './public/uploads/Kanji')
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});

var upload = multer({ //multer settings
	storage: storage
}).single('file');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {
	
	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
			return;
		}
		if (req.file) {
			try {
				var selectedKanji = req.body.selectedKanji;
				selectedKanji.audio = req.file.originalname;
				kanjiServices.createKanji(selectedKanji).then(function (result) {
					res.json({ kanji: selectedKanji });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: ex.message || ex });
			}
		} else {
			kanjiServices.createKanji(req.body).then(function (kanji) {
				res.json({ kanji: kanji });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.post('/create/list', ensureLoggedIn('/login'), function (req, res, next) {
	kanjiServices.createListKanji(req.body).then(function (kanjies) {
		res.json({ kanjies: kanjies });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {

	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
		}

		if (req.file) {
			try {
				var selectedKanji = req.body.selectedKanji;
				selectedKanji.audio = req.file.originalname;
				kanjiServices.updateKanji(selectedKanji).then(function (result) {
					res.json({ kanji: selectedKanji });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: error.message || error });
			}
		} else {
			kanjiServices.updateKanji(req.body).then(function (result) {
				res.json({ kanji: req.body });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {
	kanjiServices.deleteKanjiById(req.params.id).then(function (result) {
		res.json(result);
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/all', function (req, res, next) {
	kanjiServices.getAllKanjies().then(function (kanjies) {
		res.json({ kanjies: kanjies });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	kanjiServices.getPaginationKanjies(skip, limit).then(function (result) {
		res.json({ kanjies: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:userId/:skip/:limit', function (req, res, next) {
	const userId = req.params.userId;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	kanjiServices.getPaginationKanjiesByUser(userId, skip, limit).then(function (result) {
		res.json({ kanjies: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/:id', function (req, res, next) {
	kanjiServices.getKanjiById(req.params.id).then(function (kanjies) {
		res.json({ kanjies: kanjies });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/jp/:value', function (req, res, next) {
	kanjiServices.getKanjiByKanjiJP(req.params.value).then(function (kanji) {
		if (kanji === undefined) {
			kanji = {
				kanji_jp: req.params.value,
				kanji_vi: '',
				kanji_on:'',
				kanji_kun:'',
				level:'',
				audio:'',
				created_by: 'excel'
			}
		}
		res.json({ kanji: kanji });
	}).catch(function (error) {
		res.json({ value: req.params.value, error: error.message || error });
	});
});


module.exports = router
