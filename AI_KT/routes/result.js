const express = require('express')
const router = express.Router()
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn
const resultServices = require('../services/result.service');
const menuServices = require('../services/menu.service');
const utils = require('../helpers/utils');
const multer = require('multer');
const fs = require('fs');
const shell = require('shelljs');

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
		var selectedResult = JSON.parse(req.body.data).selectedResult;
		var path = RECORD_FOLDER + selectedResult.user + '/' + selectedResult.contentId + '/'; 
		var oldfile = JSON.parse(req.body.data).oldfile;
        if (!fs.existsSync(path)) {
			shell.mkdir('-p', path);
		}
		if (oldfile) {
			var oldFilePath = path + oldfile; 
			shell.rm('-rf', oldFilePath);
		}
        cb(null, path)
    },
    filename: function (req, file, cb) {		
        cb(null, file.originalname);
    }
});

var upload = multer({ //multer settings
    storage: storage,
}).single('file');

router.get('/detail/:id', ensureLoggedIn('/login'), function (req, res) {
	res.cookie('resultId', req.params.id);
	menuServices.getMenusByRole(req.session.passport.user.role).then(function (menus) {
		utils.render(req, res, 'pages/resultDetail', { title: 'Result Detail Page', menus: menus });
	}).catch(function (error) {
		utils.render(req, res, 'pages/errors', { title: 'Error Page', error: { message: error.message || error } });
	});	
});

router.post('/submit', ensureLoggedIn('/login'), function (req, res, next) {
	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
		} else if(req.file) {
			try {
				var selectedResult = JSON.parse(req.body.data).selectedResult;
				resultServices.submitResult(selectedResult).then(function (result) {
					res.json({ result: "Submitted" });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch(ex){
				res.json({ error: ex.message || ex });
			}
		} else {
			resultServices.submitResult(req.body).then(function (result) {
				res.json({ result: req.body });				
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.get('/all', function (req, res, next) {
	resultServices.getAllResults().then(function (results) {
		res.json({ results: results });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/:id', function (req, res, next) {
	resultServices.getResultById(req.params.id).then(function (result) {
		res.json({ result: result });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/by/:user', function (req, res, next) {
	resultServices.getResultsByUser(req.params.user).then(function (results) {
		res.json({ results: results });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/by/:user/and/:contentId', function(req, res, next){
    const user = req.params.user;
	const contentId = req.params.contentId;
	resultServices.getResultByUserAndContentId(user, contentId).then(function(result){		
		res.json({result:result});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

module.exports = router