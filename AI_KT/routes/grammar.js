const express = require('express')
const router = express.Router()
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
const grammarServices = require('../services/grammar.service');
const menuServices = require('../services/menu.service');
const utils = require('../helpers/utils');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {
	grammarServices.createGrammar(req.body).then(function (grammar) {
		res.json({ grammar: grammar });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {
	grammarServices.updateGrammar(req.body).then(function(grammar){
		res.json({grammar:grammar});
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {
	grammarServices.deleteGrammarById(req.params.id).then(function(result){		
		res.json(result);
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});


router.get('/detail/:id', ensureLoggedIn('/login'), function (req, res, next) {
	grammarServices.getGrammarById(req.params.id).then(function (grammar) {
		const username = req.session.passport.user.username;
		menuServices.getMenusByRole(req.session.passport.user.role).then(function (menus) {
			utils.render(req, res, 'pages/grammarDetail', { title: 'Grammar Detail Page', username: username, menus: menus, grammarId: grammar._id });
		}).catch(function (error) {
			utils.render(req, res, 'pages/errors', { title: 'Error Page', error: { message: error.message || error } });
		});
	}).catch(function (error) {
		utils.render(req, res, 'pages/errors', { title: 'Error Page', error: { message: error.message || error } });
	});		

});

router.get('/all', function (req, res, next) {
	grammarServices.getAllGrammars().then(function (grammars) {
		res.json({ grammars: grammars });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	grammarServices.getPaginationGrammars(skip, limit).then(function (result) {
		res.json({ grammars: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:userId/:skip/:limit', function (req, res, next) {
	const userId = req.params.userId;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	grammarServices.getPaginationGrammarsByUser(userId, skip, limit).then(function (result) {
		res.json({ grammars: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/:id', function (req, res, next) {
	grammarServices.getGrammarById(req.params.id).then(function (grammar) {
		res.json({ grammar: grammar });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

module.exports = router
