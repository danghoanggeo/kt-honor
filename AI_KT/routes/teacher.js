const express = require('express');
const router = express.Router();
const utils = require('../helpers/utils');
// Services
const testHiraganaService = require('../services/test/test.hiraga.service');
const testGimonshiService = require('../services/test/test.gimonshi.service');

// Teacher home page
router.get('/', function(req, res,next) {
    utils.render(req, res, 'pages/teacher/teacher', { title: 'Teacher', menus: [], error: "" });
});

// Teacher class
router.get('/class', function(req, res,next) {
    utils.render(req, res, 'pages/teacher/te_class', { title: 'Teacher|class', menus: [], error: "" });
});

// Teacher class
router.get('/class/:classId', function(req, res,next) {
    utils.render(req, res, 'pages/teacher/te_class_detail', { title: 'Teacher|class', menus: [], error: "" });
});

// Teacher lesson
router.get('/lesson/:levelId', function(req, res,next) {
    utils.render(req, res, 'pages/teacher/te_lesson', { title: 'Teacher|lesson', menus: [], error: "" });
});

// Teacher test
router.get('/test/:levelId', function(req, res,next) {
    utils.render(req, res, 'pages/teacher/te_test', { title: 'Teacher|test',level: req.params.levelId, menus: [], error: "" });
});

// Teacher schedule
router.get('/schedule', function(req, res,next) {
    utils.render(req, res, 'pages/teacher/te_schedule', { title: 'Teacher|test',level: req.params.levelId, menus: [], error: "" });
});

// Get student detail by ID

router.get('/gakusei/:studentId',function(req,res,next){
    utils.render(req,res, 'pages/teacher/stu_profile',{title: 'Teacher | student',error:""});
});

// API
// create testHiraganaHitotsu and send back all testHiraganaHitotsus after creation
router.post('/test/hiragana', function(req, res,next) {
    try{
        objTestHiragana= {
            quiz : req.body.quiz,
            answer : req.body.answer,
            explain:req.body.explain,
            level: req.body.level,
            lesson:req.body.lesson
        };
        console.log("------------create hiragana test---------");
        // create a writing, information comes from AJAX request from Angular
        testHiraganaService.createTestHiraga(objTestHiragana).then(function(testHiraganas){
            res.json(testHiraganas);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});

// get all testHiraganaHitotsu
router.get('/test/hiragana/:levelId', function(req, res,next) {
    try{
        var levelId  = req.params.levelId;
        testHiraganaService.getAllTestHiraga(levelId).then(function (hiraganas) {
            res.json(hiraganas);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});


// remove a testHiragana
router.post('/test/hiragana/delete',function(req,res,next){
    try {
        testHiraganaService.removeTestHiragana(req.body).then(function(testHiraganas){
            res.json(testHiraganas);
        }).catch(function(error){
            res.json({error:error.message || error});
        })
    } catch (ex) {
        res.json({error:ex.message || ex});
    }
});


// create testGimonshi and send back all testGimonshis after creation
router.post('/test/gimonshi', function(req, res,next) {
    try{
        objTestGimonshi= {
            quiz : req.body.quiz,
            answer : req.body.answer,
            explain:req.body.explain,
            level: req.body.level,
            lesson:req.body.lesson
        };
        console.log("------------create hiragana test---------");
        // create a writing, information comes from AJAX request from Angular
        testGimonshiService.createTestGimonshi(objTestGimonshi).then(function(testGimonshis){
            res.json(testGimonshis);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});

// get all testGimonshis
router.get('/test/gimonshi/:levelId', function(req, res,next) {
    try{
        var levelId  = req.params.levelId;
        testGimonshiService.getAlltestGimonshi(levelId).then(function (testGimonshis) {
            res.json(testGimonshis);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});


// remove a testGimonshi
router.post('/test/gimonshi/delete',function(req,res,next){
    try {
        testHiraganaService.removeTestHiragana(req.body).then(function(testHiraganas){
            res.json(testHiraganas);
        }).catch(function(error){
            res.json({error:error.message || error});
        })
    } catch (ex) {
        res.json({error:ex.message || ex});
    }
});


// router.post('/test/hiragana/delete', function(req, res,next) {
//     try{
//         testHiraganaService.removeTestHiragana(req.body).then(function(testHiraganas){
//             res.json(testHiraganas);
//         }).catch(function (error) {
//             res.json({ error: error.message || error });
//         });
//     }catch(ex){
//         console.log(ex);
//         res.json({ error: ex.message || ex });
//     }
// });



module.exports = router