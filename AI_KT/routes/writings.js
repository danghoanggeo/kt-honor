// writings.js
const express = require('express');
const fs = require('fs');
const router = express.Router();
const jsonData = require('../db/writing_webapp/sentences.json');
// Services
const writingServices = require('../services/writing.service');
// routes ======================================================================

Writing = require('../models/writing.model');
// api ---------------------------------------------------------------------
// get all writings
router.get('/writings', function(req, res,next) {
    try{
        writingServices.getAllWritings().then(function (writings) {
            res.json(writings);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});

router.get('/sentences',function(req,res,next){
    try{
        
        writingServices.getAllSentences().then(function(data){
            res.json(data);
        }).catch(function(error){
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({error:ex.message});
    }
});

// create writing and send back all writings after creation
router.post('/writings', function(req, res,next) {
    try{
        objWriting = {
            text : req.body.text,
            done : false
        };
        // create a writing, information comes from AJAX request from Angular
        writingServices.createWriting(objWriting).then(function(writings){
            res.json(writings);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
        
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});

// update writing and send back all writings after updated
router.post('/writings/:writing_id', function(req, res,next) {
    Writing = new Writing({
        text : req.body.text,
        done : false
    });
    Writing.save(function(error) {
        console.log("Your bee has been saved!");
        if (error) {
            console.error(error);
            res.send(error);
        }
        // get and return all the writings after you create another
        Writing.find(function(err, writings) {
            if (err)
                res.send(err);
            res.json(writings);
        });
    });
    // create a writing, information comes from AJAX request from Angular

});

// delete a writing
router.delete('/writings/:writing_id', function(req, res,next) {
    var writingId  = req.params.writing_id;
    writingServices.removeWriting(writingId,(error,writings)=>{
        if(error)
            res.json({ error: error.message || error });
        res.json(writings);
    });
});

// get analyze a sentences
router.post('/sentence/analyze', function(req, res,next) {
    try{
        var sentence = req.body.sentence;
        var index = req.body.index;
        console.log(req.body);
        writingServices.analyzeWriting(index,sentence).then(function (results) {
            res.json(results);
        }).catch(function (error) {
            res.json({ error: error.message || error });
        });
    }catch(ex){
        res.json({ error: ex.message || ex });
    }
});

module.exports = router