const express = require('express')
const router = express.Router()
const searchServices = require('../services/search.service');
const utils = require('../helpers/utils');

router.get('/:name', function (req, res, next) {
	searchServices.getContentByName(req.params.name).then(function (contents) {
        res.json({ contents: contents });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:name/:skip/:limit', function(req, res, next){
	const name = req.params.name;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	searchServices.getPaginationSearchResult(name,skip, limit).then(function(result){		
		res.json({contents:result.docs, total: utils.getPaginateTotalResult(result.metadatas)});		
		res.json({error: error.message || error});
	});
});
module.exports = router