const express = require('express')
const router = express.Router()
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn
const specialServices = require('../services/special.service');
const utils = require('../helpers/utils');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {
    specialServices.saveOrUpdateSpecial(req.body).then(function (special) {
				res.json({ special: req.body });				
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
});

router.get('/all', function (req, res, next) {
	specialServices.getAllSpecials().then(function (specials) {
		res.json({ specials: specials });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/by/:user/and/:contentId', function(req, res, next){
    const user = req.params.user;
	const contentId = req.params.contentId;
	specialServices.getSpecialByUserAndContentId(user, contentId).then(function(special){		
		res.json({special:special});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

module.exports = router