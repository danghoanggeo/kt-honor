const express = require('express')
const router = express.Router()
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn
const contentServices = require('../services/content.service');
const resultServices = require('../services/result.service');
const specialServices = require('../services/special.service');
const menuServices = require('../services/menu.service');
const utils = require('../helpers/utils');
const multer = require('multer');

var storage = multer.diskStorage({ //multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './public/uploads/Video')
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});

var upload = multer({ //multer settings
	storage: storage,
}).single('file');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {

	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
			return;
		}
		if (req.file) {
			try {
				var selectedContent = JSON.parse(req.body.selectedContent);
				selectedContent.video = req.file.originalname;
				contentServices.createContent(selectedContent).then(function (result) {
					res.json({ content: selectedContent });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: ex.message || ex });
			}
		} else {
			contentServices.createContent(req.body).then(function (result) {
				res.json({ content: req.body });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.post('/create/list', function (req, res, next) {
	contentServices.createListContent(req.body).then(function (contents) {
		res.json({ contents: contents });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {

	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
		}

		if (req.file) {
			try {
				var selectedContent = JSON.parse(req.body.selectedContent);
				selectedContent.video = req.file.originalname;
				contentServices.updateContent(selectedContent).then(function (result) {
					res.json({ content: selectedContent });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: error.message || error });
			}
		} else {
			contentServices.updateContent(req.body).then(function (result) {
				res.json({ content: req.body });
			}).catch(function (error) {
				res.json({ error: error.message || error });
				console.log(error);
			});
		}
	})
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {
	contentServices.deleteContentById(req.params.id).then(function (result) {
		res.json(result);
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/all', function (req, res, next) {
	contentServices.getAllContents().then(function (contents) {
		res.json({ contents: contents });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/root', function (req, res, next) {
	contentServices.getRootContents().then(function (contents) {
		res.json({ contents: contents });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
})

router.get('/root/paginate/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	contentServices.getPaginationRootContents(skip, limit).then(function (result) {
		result.docs.forEach(element => {
			if (element.lessons && element.lessons.length > 0) {
				element.lessonName = element.lessons[0].name;
				delete element.lessons;
			}
		});
		res.json({ contents: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
})

router.get('/paginate/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	contentServices.getPaginationContents(skip, limit).then(function (result) {
		result.docs.forEach(element => {
			if (element.lessons && element.lessons.length > 0) {
				element.lessonName = element.lessons[0].name;
				delete element.lessons;
			}
		});
		res.json({ contents: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:userId/:skip/:limit', function (req, res, next) {
	const userId = req.params.userId;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	contentServices.getPaginationContentsByUser(userId, skip, limit).then(function (result) {
		res.json({ contents: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/detail/:id', ensureLoggedIn('/login'), function (req, res, next) {
	res.cookie('contentId', req.params.id);
	menuServices.getMenusByRole(req.session.passport.user.role).then(function (menus) {
		utils.render(req, res, 'pages/contentDetail', { title: 'Content Detail Page', menus: menus });
	}).catch(function (error) {
		utils.render(req, res, 'pages/errors', { title: 'Error Page', error: { message: error.message || error } });
	});	
});

router.get('/:id', function (req, res, next) {
	contentServices.getContentById(req.params.id).then(function (content) {
		res.json({ content: content });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/lesson/:value', function (req, res, next) {
	contentServices.getContentByLesson(req.params.value).then(function (contents) {
		res.json({ contents: contents });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/pagination/:_id/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	const _id = req.params._id;
	contentServices.getPaginationLessonDetails(_id, skip, limit).then(function (result) {
		result.docs.forEach(element => {
			if (element.lessons && element.lessons.length > 0) {
				element.lessonName = element.lessons[0].name;
				delete element.lessons;
			}
		});
		res.json({ contents: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/pagination/:username/:_id/:skip/:limit', function (req, res, next) {
	const username = req.params.username;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	const _id = req.params._id;
	contentServices.getPaginationContentsByLessonByUser(username, _id, skip, limit).then(function (result) {
		res.json({ contents: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/by/:user/and/:id', function (req, res, next) {
	var contentId = req.params.id;
	var user = req.params.user;
	contentServices.getContentById(contentId).then(function (content) {
		resultServices.getResultByUserAndContentId(user, contentId).then(function(result){	
			specialServices.getSpecialByUserAndContentId(user, contentId).then(function(special){		
				res.json({content:content, special:special, result:result});		
			}).catch(function(error){
				res.json({error: error.message || error});
			});			
		}).catch(function(error){
			res.json({error: error.message || error});
		});
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

module.exports = router