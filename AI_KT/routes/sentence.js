const express = require('express')
const router = express.Router()
const passport = require('../helpers/passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
const sentenceServices = require('../services/sentence.service');
const utils = require('../helpers/utils');
const fs = require('fs');
const multer = require('multer');

var storage = multer.diskStorage({ //multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './public/uploads/Sentence')
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});

var upload = multer({ //multer settings
	storage: storage
}).single('file');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {
	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
			return;
		}
		if (req.file) {
			try {
				var selectedSentence = req.body.selectedSentence;
				selectedSentence.audio = req.file.originalname;
				sentenceServices.createSentence(selectedSentence).then(function (result) {
					res.json({ sentence: selectedSentence });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: ex.message || ex });
			}
		} else {
			sentenceServices.createSentence(req.body).then(function (sentence) {
				res.json({ sentence: sentence });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.post('/create/list', ensureLoggedIn('/login'), function (req, res, next) {
	sentenceServices.createListSentence(req.body).then(function (sentences) {
	 res.json({ sentences: sentences });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {
	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
		}

		if (req.file) {
			try {
				var selectedSentence = req.body.selectedSentence;
				selectedSentence.audio = req.file.originalname;
				sentenceServices.updateSentence(selectedSentence).then(function (result) {
					res.json({ sentence: selectedSentence });
				}).catch(function (error) {
					res.json({ error: error.message || error });
					console.log(error);
				});
			} catch (ex) {
				res.json({ error: error.message || error });
			}
		} else {
			sentenceServices.updateSentence(req.body).then(function (result) {
				res.json({ sentence: req.body });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.get('/delete/:sentence_jp', ensureLoggedIn('/login'), function (req, res, next) {
	sentenceServices.deleteSentenceBySentenceJP(req.params.sentence_jp).then(function (result) {
		res.json(result);
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/all', function (req, res, next) {
	sentenceServices.getAllSentences().then(function (sentences) {
		res.json({ sentences: sentences });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	sentenceServices.getPaginationSentences(skip, limit).then(function (result) {
		res.json({ sentences: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:userId/:skip/:limit', function (req, res, next) {
	const userId = req.params.userId;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	sentenceServices.getPaginationSentencesByUser(userId, skip, limit).then(function (result) {
		res.json({ sentences: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/:id', function (req, res, next) {
	sentenceServices.getSentenceById(req.params.id).then(function (sentences) {
		res.json({ sentences: sentences });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/jp/:value', function (req, res, next) {
	sentenceServices.getSentenceBySentenceJP(req.params.value).then(function (sentence) {
		if (sentence === undefined) {
			sentence = {
				sentence_jp: req.params.value,
				sentence_vi: '',
				startTime: '',
				endTime: '',
				audio: '',
				created_by: 'excel'
			}
		}
		res.json({ sentence: sentence });
	}).catch(function (error) {
		res.json({ value: req.params.value, error: error.message || error });
	});
});


module.exports = router
