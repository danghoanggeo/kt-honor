const express = require('express')
const router = express.Router()
const passport = require('../helpers/passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
const wordServices = require('../services/word.service');
const multer = require('multer');
const utils = require('../helpers/utils');

var storage = multer.diskStorage({ //multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './public/uploads/Word')
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});

var upload = multer({ //multer settings
	storage: storage
}).single('file');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {
	
	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
			return;
		}
		if (req.file) {
			try {
				var selectedWord = req.body.selectedWord;
				selectedWord.audio = req.file.originalname;
				wordServices.createWord(selectedWord).then(function (result) {
					res.json({ word: selectedWord });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: ex.message || ex });
			}
		} else {
			wordServices.createWord(req.body).then(function (word) {
				res.json({ word: word });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.post('/create/list', ensureLoggedIn('/login'), function (req, res, next) {
	wordServices.createListWord(req.body).then(function (words) {
		res.json({ words: words });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {

	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
		}

		if (req.file) {
			try {
				var selectedWord = req.body.selectedWord;
				selectedWord.audio = req.file.originalname;
				wordServices.updateWord(selectedWord).then(function (result) {
					res.json({ word: selectedWord });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: error.message || error });
			}
		} else {
			wordServices.updateWord(req.body).then(function (result) {
				res.json({ word: req.body });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {
	wordServices.deleteWordById(req.params.id).then(function (result) {
		res.json(result);
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/all', function (req, res, next) {
	wordServices.getAllWords().then(function (words) {
		res.json({ words: words });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	wordServices.getPaginationWords(skip, limit).then(function (result) {
		res.json({ words: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:userId/:skip/:limit', function (req, res, next) {
	const userId = req.params.userId;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	wordServices.getPaginationWordsByUser(userId, skip, limit).then(function (result) {
		res.json({ words: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/:id', function (req, res, next) {
	wordServices.getWordById(req.params.id).then(function (words) {
		res.json({ words: words });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});
router.get('/jp/:value', function (req, res, next) {
	wordServices.getWordByWordJP(req.params.value).then(function (word) {
		if (word === undefined) {
			word = {
				word_jp: req.params.value,
				word_vi: '',
				kanji:'',
				level:'',
				audio:'',
				created_by: 'excel'
			}
		}
		res.json({ word: word });
	}).catch(function (error) {
		res.json({ value: req.params.value, error: error.message || error });
	});
});


module.exports = router
