const express = require('express')
const router = express.Router()
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn
const quizServices = require('../services/quiz.service');
const menuServices = require('../services/menu.service');
const utils = require('../helpers/utils');
const multer = require('multer');

var storage = multer.diskStorage({ //multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './public/uploads/Sentence')
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});

var upload = multer({ //multer settings
	storage: storage,
}).single('file');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {

	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
			return;
		}
		if (req.file) {
			try {
				var selectedQuiz = JSON.parse(req.body.selectedQuiz);
				selectedQuiz.audio = req.file.originalname;
				quizServices.createQuiz(selectedQuiz).then(function (result) {
					res.json({ quiz: selectedQuiz });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: ex.message || ex });
			}
		} else {
			quizServices.createQuiz(req.body).then(function (result) {
				res.json({ quiz: req.body });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {

	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
		}

		if (req.file) {
			try {
				var selectedQuiz = JSON.parse(req.body.selectedQuiz);
				selectedQuiz.audio = req.file.originalname;
				quizServices.updateQuiz(selectedQuiz).then(function (result) {
					res.json({ quiz: selectedQuiz });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: error.message || error });
			}
		} else {
			quizServices.updateQuiz(req.body).then(function (result) {
				res.json({ quiz: req.body });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {
	quizServices.deleteQuizById(req.params.id).then(function (result) {
		res.json(result);
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/all', function (req, res, next) {
	quizServices.getAllQuizes().then(function (quizes) {
		res.json({ quizes: quizes });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	quizServices.getPaginationQuizes(skip, limit).then(function (result) {
		res.json({ quizes: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:userId/:skip/:limit', function (req, res, next) {
	const userId = req.params.userId;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	quizServices.getPaginationQuizesByUser(userId, skip, limit).then(function (result) {
		res.json({quizes: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

module.exports = router