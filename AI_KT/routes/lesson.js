const express = require('express')
const router = express.Router()
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn
const lessonServices = require('../services/lesson.service');
const menuServices = require('../services/menu.service');
const utils = require('../helpers/utils');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {			
	lessonServices.createLesson(req.body).then(function(lesson){
		res.json({lesson:lesson});
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});
router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {	
	lessonServices.updateLesson(req.body).then(function(lesson){
		res.json({lesson:lesson});
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {		
	lessonServices.deleteLessonById(req.params.id).then(function(result){		
		res.json(result);
		
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.get('/all', function(req, res, next){
	lessonServices.getAllLessons().then(function(lessons){
		res.json({lessons:lessons});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/paginate/:skip/:limit', function(req, res, next){
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	lessonServices.getPaginationLessons(skip, limit).then(function(result){		
		res.json({lessons:result.docs, total: utils.getPaginateTotalResult(result.metadatas)});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/paginate/:userId/:skip/:limit', function(req, res, next){
    const userId = req.params.userId;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	lessonServices.getPaginationLessonsByUser(userId, skip, limit).then(function(result){		
		res.json({lessons:result.docs, total: utils.getPaginateTotalResult(result.metadatas)});
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/detail/:id', ensureLoggedIn('/login'), function(req, res, next){		
	lessonServices.getLessonById(req.params.id).then(function(lesson){	
		const username = req.session.passport.user.username;	
		menuServices.getMenusByRole(req.session.passport.user.role).then(function(menus) {	
			var options = { title: 'List content', username: username, menus: menus, lessonId : lesson._id };
			utils.render(req, res, 'pages/lessonDetail', options);
		}).catch(function(error){
			utils.render(req, res, 'pages/errors', { title: 'Error Page', error: { message: error.message || error } });
		});
	}).catch(function(error){			
		utils.render(req, res, 'pages/errors', { title: 'Error Page', error: { message: error.message || error } });
	});
});
router.get('/:id', function(req, res, next){
	lessonServices.getLessonById(req.params.id).then(function(lesson){
		res.json({lesson:lesson});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});
router.get('/content/:name', function(req, res, next){
	lessonServices.getLessonByName(req.params.name).then(function(lessons){
		res.json({lessons:lessons});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});
router.get('/level/:level', function(req, res, next){
	lessonServices.getLessonsByLevel(req.params.level).then(function(lessons){
		res.json({lessons:lessons});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

module.exports = router;