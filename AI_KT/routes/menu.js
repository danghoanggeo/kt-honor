const express = require('express')
const router = express.Router()
const passport = require('../helpers/passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn
const menuServices = require('../services/menu.service');
const utils = require('../helpers/utils');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {		
	menuServices.createMenu(req.body).then(function(menu){
		res.json({menu:menu});
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {
	menuServices.updateMenu(req.body).then(function(menu){
		res.json({menu:menu});
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {		
	menuServices.deleteMenuById(req.params.id).then(function(result){		
		res.json(result);
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.get('/all', function(req, res, next){
	menuServices.getAllMenus().then(function(menus){
		res.json({menus:menus});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/allSuper', function(req, res, next){
	menuServices.getAllMenuSuper().then(function(supers){
		res.json({supers:supers});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/role/:role', function(req, res, next){
	menuServices.getMenusByRole(req.params.role).then(function(menus){
		res.json({menus:menus});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/paginate/:skip/:limit', function(req, res, next){
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	menuServices.getPaginationMenus(skip, limit).then(function(result){		
		res.json({menus:result.docs, total: utils.getPaginateTotalResult(result.metadatas)});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

module.exports = router