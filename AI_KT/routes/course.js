const express = require('express')
const router = express.Router()
const passport = require('../helpers/passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn
const courseServices = require('../services/course.service');
const utils = require('../helpers/utils');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {		
	courseServices.createCourse(req.body).then(function(course){
		res.json({course:course});
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {
	courseServices.updateCourse(req.body).then(function(course){
		res.json({course:course});
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {		
	courseServices.deleteCourseById(req.params.id).then(function(result){		
		res.json(result);
	}).catch(function(error){
		res.json({error:error.message || error});
	});				
});

router.get('/all', function(req, res, next){
	courseServices.getAllCourses().then(function(courses){
		res.json({courses:courses});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/paginate/:skip/:limit', function(req, res, next){
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	courseServices.getPaginationCourses(skip, limit).then(function(result){		
		res.json({courses:result.docs, total: utils.getPaginateTotalResult(result.metadatas)});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

router.get('/paginate/:username/:skip/:limit', function(req, res, next){
    const username = req.params.username;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	courseServices.getPaginationCoursesByUser(username, skip, limit).then(function(result){		
		res.json({courses:result.docs, total: utils.getPaginateTotalResult(result.metadatas)});		
	}).catch(function(error){
		res.json({error: error.message || error});
	});
});

module.exports = router