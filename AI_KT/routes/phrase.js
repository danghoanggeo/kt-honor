const express = require('express')
const router = express.Router()
const passport = require('../helpers/passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
const phraseServices = require('../services/phrase.service');
const multer = require('multer');
const utils = require('../helpers/utils');

var storage = multer.diskStorage({ //multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './public/uploads/Phrase')
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});

var upload = multer({ //multer settings
	storage: storage
}).single('file');

router.post('/create', ensureLoggedIn('/login'), function (req, res, next) {

	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
			return;
		}
		if (req.file) {
			try {
				var selectedPhrase = req.body.selectedPhrase;
				selectedPhrase.audio = req.file.originalname;
				phraseServices.createPhrase(selectedPhrase).then(function (result) {
					res.json({ phrase: selectedPhrase });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: ex.message || ex });
			}
		} else {
			phraseServices.createPhrase(req.body).then(function (phrase) {
				res.json({ phrase: phrase });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.post('/create/list', ensureLoggedIn('/login'), function (req, res, next) {
	phraseServices.createListPhrase(req.body).then(function (phrases) {
		res.json({ phrases: phrases });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.post('/update', ensureLoggedIn('/login'), function (req, res, next) {

	upload(req, res, function (error) {
		if (error) {
			res.json({ error: error.message || error });
		}

		if (req.file) {
			try {
				var selectedPhrase = req.body.selectedPhrase;
				selectedPhrase.audio = req.file.originalname;
				phraseServices.updatePhrase(selectedPhrase).then(function (result) {
					res.json({ phrase: selectedPhrase });
				}).catch(function (error) {
					res.json({ error: error.message || error });
				});
			} catch (ex) {
				res.json({ error: error.message || error });
			}
		} else {
			phraseServices.updatePhrase(req.body).then(function (result) {
				res.json({ phrase: req.body });
			}).catch(function (error) {
				res.json({ error: error.message || error });
			});
		}
	})
});

router.get('/delete/:id', ensureLoggedIn('/login'), function (req, res, next) {
	phraseServices.deletePhraseById(req.params.id).then(function (result) {
		res.json(result);
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/all', function (req, res, next) {
	phraseServices.getAllPhrases().then(function (phrases) {
		res.json({ phrases: phrases });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:skip/:limit', function (req, res, next) {
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	phraseServices.getPaginationPhrases(skip, limit).then(function (result) {
		res.json({ phrases: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/paginate/:userId/:skip/:limit', function (req, res, next) {
	const userId = req.params.userId;
	const skip = parseInt(req.params.skip);
	const limit = parseInt(req.params.limit);
	phraseServices.getPaginationPhrasesByUser(userId, skip, limit).then(function (result) {
		res.json({ phrases: result.docs, total: utils.getPaginateTotalResult(result.metadatas) });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});

router.get('/:id', function (req, res, next) {
	phraseServices.getPhraseById(req.params.id).then(function (phrases) {
		res.json({ phrases: phrases });
	}).catch(function (error) {
		res.json({ error: error.message || error });
	});
});
router.get('/jp/:value', function (req, res, next) {
	phraseServices.getphraseByphraseJP(req.params.value).then(function (phrase) {
		if (phrase === undefined) {
			phrase = {
				phrase_jp: req.params.value,
				phrase_vi: '',
				startTime: '',
				endTime: '',
				audio: '',
				created_by: 'excel'
			}
		}
		res.json({ phrase: phrase });
	}).catch(function (error) {
		res.json({ value: req.params.value, error: error.message || error });
	});
});


module.exports = router
