var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContentSchema = new Schema({
    name : {
        type: String,
        index : true,
        unique: true,
        require:true
    },
    view:String,
    actors: [],
    video:String,
    subtitle:String,
    order: Number,  
    level:String, 
    sentences : [],
    sentenceCustoms : [],
    words: [],
    phrases:[],
    kanjies:[],
    quizes: [],
    grammars: [],
    created_date : {
        type: Date, 
        default: Date.now
    },
    updated_date : {
        type: Date, 
        default: Date.now
    },
    created_by : String,
    lesson:{type: Schema.Types.ObjectId, ref: 'Contents'},
    status : String,    
    parent: String
});

module.exports = mongoose.model('Content', ContentSchema);