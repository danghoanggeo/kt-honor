var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LessonSchema = new Schema({
    name : {
        type: String,
        index : true,
        unique: true,
        required: true
    },
    url : String,
    level:String,
    order: Number,   
    created_date : {
        type: Date, 
        default: Date.now
    },
    updated_date : {
        type: Date, 
        default: Date.now
    },
    created_by : String,
    description : String
});

module.exports = mongoose.model('Lesson', LessonSchema);