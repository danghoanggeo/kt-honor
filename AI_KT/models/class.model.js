var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClassSchema = new Schema({
    name : {
        type: String,
        index : true,
        unique: true,
        required: true
    },
    level: String,        
    teachers: [],
    students: [],
    lessons:[],
    start_date:{
        type:Date,
        default: Date.now
    } ,
    end_date:{
        type:Date,
        default: Date.now
    } ,
    status: String,
    created_by : String,
    created_date : {
        type: Date, 
        default: Date.now
    },
    updated_date : {
        type: Date, 
        default: Date.now
    }
});

module.exports = mongoose.model('Class', ClassSchema);