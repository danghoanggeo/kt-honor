var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DeviceSchema = new Schema({
    name: String,
    deviceId: {
        type: String,
        index : true,
        unique: true,
        require:true
    },
    created_date : { 
        type   : Date, 
        default: Date.now 
    }, 
    updated_date : { 
        type   : Date, 
        default: Date.now 
    }, 
});

module.exports = mongoose.model('Device', DeviceSchema);