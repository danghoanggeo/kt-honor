var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuizSchema = new Schema({
    question_jp : {
        type  : String,        
        index : true,
        unique: true,
        required: true
    },
    question_vi : String,
    audio : String,
    responses : [],
    created_date : {
        type: Date, 
        default: Date.now
    },
    updated_date : {
        type: Date, 
        default: Date.now
    },
    created_by : String,
    status : String,    
    level: String
});

module.exports = mongoose.model('Quiz', QuizSchema);