var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CourseSchema = new Schema({
    name : {
        type: String,
        index : true,
        unique: true,
        required: true
    },
    price : Number,    
    created_by : String,
    start_date: Date,
    end_date: Date,
    status : String,
    created_date : {
        type: Date, 
        default: Date.now
    },
    updated_date : {
        type: Date, 
        default: Date.now
    }
});

module.exports = mongoose.model('Course', CourseSchema);