var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TestGimonshi = new Schema({
    quiz : String,
    answer:String,
    num_used: {
        type:Number,
        default:0
    },
    num_correct:{
        type:Number,
        default:0
    },
    explain:  {
        type:String,
        default:"ありません"
    },
    level: String,
    lesson:String,
    created_by: {
        type:String,
        default:"0"
    },
    updated_date: {
        type: Date, 
        default: Date.now
    },
    status:  {
        type: Boolean, 
        default: true
    }
});

module.exports = mongoose.model('TestGimonshi', TestGimonshi);