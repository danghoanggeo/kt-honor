var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TestHiragaHitotsu = new Schema({
    quiz : String,
    answer:[],
    num_used: {
        type:Number,
        default:0
    },
    num_correct:{
        type:Number,
        default:0
    },
    wrongAnswers:{
       type: [],
       default:[]
    },
    explain:  {
        type:String,
        default:"ありません"
    },
    level: String,
    lesson:String,
    created_by: {
        type:String,
        default:"0"
    },
    updated_date: {
        type: Date, 
        default: Date.now
    },
    status:  {
        type: Boolean, 
        default: true
    }
});

module.exports = mongoose.model('TestHiragaHitotsu', TestHiragaHitotsu);