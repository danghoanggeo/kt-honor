var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var WordSchema = new Schema({ 
    word_jp : {
        type  : String,        
        index : true,
        unique: true,
    }, 
    word_vi : {
        type  : String,        
        index : true
    },  
    kanji:String,
    level:String,
    audio : String, 
    created_date : { 
        type: Date, 
        default: Date.now 
    }, 
    updated_date : { 
        type: Date, 
        default: Date.now 
    }, 
    created_by : String    
});

module.exports = mongoose.model('Word', WordSchema);