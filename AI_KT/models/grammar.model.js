var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var GrammarSchema = new Schema({ 
    title : {
        type  : String,        
        index : true,
        unique: true,
    }, 
    description : String,
    level:String,
    created_date : { 
        type: Date, 
        default: Date.now 
    }, 
    updated_date : { 
        type: Date, 
        default: Date.now 
    }, 
    created_by : String    
});

module.exports = mongoose.model('Grammar', GrammarSchema);