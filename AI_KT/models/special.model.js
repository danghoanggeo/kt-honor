var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var SpecialSchema = new Schema({
    user: String,
    words:[],
    phrases:[],
    quizes: [],
    contentId: String,
    created_by: String,
    created_date: {
        type: Date, 
        default: Date.now
    },
    updated_date: {
        type: Date, 
        default: Date.now
    }
});

module.exports = mongoose.model('Special', SpecialSchema);