var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var KaiwaTopicSchema = new Schema({
    topic : String,
    description: String,
    created_by: String,
    updated_date: {
        type: Date, 
        default: Date.now
    },
    status:  {
        type: Boolean, 
        default: false
    }
});

module.exports = mongoose.model('KaiwaTopic', KaiwaTopicSchema);