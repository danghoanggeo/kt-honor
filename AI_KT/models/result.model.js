var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ResultSchema = new Schema({
    user: String,   
    words:[],
    phrases:[],
    quizes: [],
    sentences: [],
    alexa: [],
    comment: String,
    commented_by: String,
    studiedVideo:{
        type:Boolean,
        default:false
    } ,
    studiedRemember: {
        type:Boolean,
        default:false
    } ,
    studiedSpeak: {
        type:Boolean,
        default:false
    } ,
    contentId: String,
    contentName: String,
    start_date : {
        type: Date, 
        default: Date.now
    },
    end_date : {
        type: Date, 
        default: Date.now
    }
});

module.exports = mongoose.model('Result', ResultSchema);