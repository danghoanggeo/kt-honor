var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MenuSchema = new Schema({
    name : {
        type: String,
        index : true
    },
    icon : String,
    link : String,
    roles : [String], 
    super :String,
    order: Number,           
    created_date : {
        type: Date, 
        default: Date.now
    },
    updated_date : {
        type: Date, 
        default: Date.now
    },	
});

module.exports = mongoose.model('Menu', MenuSchema);





