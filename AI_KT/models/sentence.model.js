var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var SentenceSchema = new Schema({ 
    sentence_jp : {
        type  : String,        
        index : true,
        unique: true,
    }, 
    sentence_vi : String,
    startTime: {
        type : Number,
        default : 0
    },    
    endTime: {
        type : Number,
        default : 0
    },    
    audio : String, 
    created_date : { 
        type: Date, 
        default: Date.now 
    }, 
    updated_date : { 
        type: Date, 
        default: Date.now 
    }, 
    created_by : String    
});

module.exports = mongoose.model('Sentence', SentenceSchema);