var mongoose = require('mongoose');


// define model =================
var schema = mongoose.Schema({
    text : {type:String , required:true},
    user_id: String,
    sentence_id:String,
    timestamp:{type:Date, default: Date.now},
    score: {
        type : Number,
        default : 0
    },    
    mistake:String,
    done: {type:Boolean , default: true}
})

module.exports = mongoose.model('Writing', schema);