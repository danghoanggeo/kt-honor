var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var PhraseSchema = new Schema({ 
    phrase_jp : {
        type  : String,        
        index : true,
        unique: true,
    }, 
    phrase_vi : String,
    kanji:String,
    level:String,
    audio : String, 
    created_date : { 
        type: Date, 
        default: Date.now 
    }, 
    updated_date : { 
        type: Date, 
        default: Date.now 
    }, 
    created_by : String    
});

module.exports = mongoose.model('Phrase', PhraseSchema);