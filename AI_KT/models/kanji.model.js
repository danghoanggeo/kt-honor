var mongoose = require('mongoose'); 
var Schema = mongoose.Schema; 

var KanjiSchema = new Schema({ 
    kanji_jp : {
        type  : String,        
        index : true,
        unique: true,
    }, 
    kanji_vi  : String,
    kanji_on  : String,
    kanji_kun : String,
    level     : String,
    created_date : { 
        type   : Date, 
        default: Date.now 
    }, 
    updated_date : { 
        type   : Date, 
        default: Date.now 
    }, 
    created_by : String    
});

module.exports = mongoose.model('Kanji', KanjiSchema);