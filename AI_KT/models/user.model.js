var mongoose = require('mongoose');
var bcrypt   = require('bcryptjs');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    fullname : {
        type  : String
    },
    avatar: String,
    username : {
        type  : String,        
        index : true,
        unique: true,
        required: true
    },    
    password : {
        type  : String,
        required: true
    },
    email : {
        type  : String,        
        index : true,
        unique: true
    },
    role : String,
    day_of_birth : Date,
    token : {
        type : String
    },
    created_date : {
        type: Date, 
        default: Date.now
    },
    updated_date : {
        type: Date, 
        default: Date.now
    },
    expired_date : {
        type : Date
    },
    active : {
        type : Boolean
    },
    resetPasswordToken: String,
    resetPasswordExpires: Date	
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, 10);
};

UserSchema.methods.verifyPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);





