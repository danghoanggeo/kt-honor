var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ActivitySchema = new Schema({
    name : String,
    user: String,   
    addition: {},
    href: String,
    description: String,
    created_by: String,
    updated_date: {
        type: Date, 
        default: Date.now
    },
    confirmed:  {
        type: Boolean, 
        default: false
    }
});

module.exports = mongoose.model('Activity', ActivitySchema);