#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-*- coding: utf-8 -*-
#-*- coding: uec-kr -*-
import context
import paho.mqtt.client as mqtt
import json as j
import re

SENT_USER_KEY = "demo/mqtt-hack-ai"
USER_RECEIVED_CONFIRM ="mqtt/demo-kt-confirm"

def convertDataToJson(data):
    fr = data.index("{")
    en = data.index("}") + 1
    str = data[fr:en]
    jdata = j.loads(str)
    return jdata

def on_connect(mqttc,obj,flags,rc):
    print("rc: " + str(rc))

def on_message(mqttc,obj,msg):
    print(str(msg.payload))
    jdata = convertDataToJson(str(msg.payload))
    print(jdata)

def on_publish(mqttc,obj,mid):
    print("mind: " + str(mid))

def on_subscribe(mqttc,obj,mid,granted_qos):
    print("Subscribed: "+ str(mid)+" "+str(granted_qos))

def on_log(mqttc, obj,level, string):
    print(string)

mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

mqttc.connect("test.mosquitto.org",1883,60)
mqttc.subscribe(USER_RECEIVED_CONFIRM,0)

mqttc.loop_forever()
