import RPi.GPIO as GPIO
import time

LedPin = 31    # pin11
button = 29
def setup():
  GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location
  GPIO.setup(LedPin, GPIO.OUT)   # Set LedPin's mode is output
  GPIO.output(LedPin, GPIO.HIGH) # Set LedPin high(+3.3V) to turn on led
  GPIO.setup(button, GPIO.IN) # Set LedPin high(+3.3V) to turn on led
  
def blink():
    while True:
        if not (GPIO.input(button)):
            GPIO.output(LedPin, GPIO.HIGH)  # led on
        GPIO.output(LedPin, GPIO.LOW) # led off

def destroy():
  GPIO.output(LedPin, GPIO.LOW)   # led off
  GPIO.cleanup()                  # Release resource

if __name__ == '__main__':     # Program start from here
  setup()
  try:
    blink()
  except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
    destroy()