#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import grpc

import gigagenieRPC_pb2
import gigagenieRPC_pb2_grpc

import os
import datetime
import hmac
import hashlib

import context
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json as j
import re
from _player import WavePlayer


SENT_USER_KEY = "demo/mqtt-hack-ai"
USER_RECEIVED_CONFIRM ="mqtt/demo-kt-confirm"


# Config for GiGA Genie gRPC
CLIENT_ID = 'Y2xpZW50X2lkMTU0Nzg3MTAwNzYxNA=='
CLIENT_KEY = 'Y2xpZW50X2tleTE1NDc4NzEwMDc2MTQ='
CLIENT_SECRET = 'Y2xpZW50X3NlY3JldDE1NDc4NzEwMDc2MTQ='
HOST = 'gate.gigagenie.ai'
PORT = 4080

player = WavePlayer()
with open('data_conver.json') as f:
    con_data = j.load(f)
    print(con_data)

def getMetadata():
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")[:-3]
    message = CLIENT_ID + ':' + timestamp

    signature = hmac.new(CLIENT_SECRET.encode(), message.encode(), hashlib.sha256).hexdigest()

    metadata = [('x-auth-clientkey', CLIENT_KEY),
                ('x-auth-timestamp', timestamp),
                ('x-auth-signature', signature)]

    return metadata

def credentials(context, callback):
    callback(getMetadata(), None)

def getCredentials():
    with open('../data/ca-bundle.pem', 'rb') as f:
        trusted_certs = f.read()
    sslCred = grpc.ssl_channel_credentials(root_certificates=trusted_certs)

    authCred = grpc.metadata_call_credentials(credentials)

    return grpc.composite_channel_credentials(sslCred, authCred)

### END OF COMMON ###

### STT
import pyaudio
import audioop
from six.moves import queue

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 12000
CHUNK = 512

# MicrophoneStream - original code in https://goo.gl/7Xy3TT
class MicrophoneStream(object):
    """Opens a recording stream as a generator yielding the audio chunks."""
    def __init__(self, rate, chunk):
        self._rate = rate
        self._chunk = chunk

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

    def __enter__(self):
        self._audio_interface = pyaudio.PyAudio()
        self._audio_stream = self._audio_interface.open(
            format=pyaudio.paInt16,
            channels=1, rate=self._rate,
            input=True, frames_per_buffer=self._chunk,
            # Run the audio stream asynchronously to fill the buffer object.
            # This is necessary so that the input device's buffer doesn't
            # overflow while the calling thread makes network requests, etc.
            stream_callback=self._fill_buffer,
        )

        self.closed = False

        return self

    def __exit__(self, type, value, traceback):
        self._audio_stream.stop_stream()
        self._audio_stream.close()
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        self._audio_interface.terminate()

    def _fill_buffer(self, in_data, frame_count, time_info, status_flags):
        """Continuously collect data from the audio stream, into the buffer."""
        self._buff.put(in_data)
        return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)
# [END audio_stream]

def generate_request():
    i = 0

    with MicrophoneStream(RATE, CHUNK) as stream:
        audio_generator = stream.generator()
    
        for content in audio_generator:
            i += 1
            message = gigagenieRPC_pb2.reqVoice()
            message.audioContent = content
            yield message
        print('Number of content: {}'.format(i))
def getVoice2Text():	

    print ("Ctrl+\ to quit ...")
	
    channel = grpc.secure_channel('{}:{}'.format(HOST, PORT), getCredentials())
    stub = gigagenieRPC_pb2_grpc.GigagenieStub(channel)
    request = generate_request()
    resultText = ''
    for response in stub.getVoice2Text(request):
        if response.resultCd == 200: # partial
            print('resultCd=%d | recognizedText= %s' 
                  % (response.resultCd, response.recognizedText))
            resultText = response.recognizedText
            break
        elif response.resultCd == 201: # final
            print('resultCd=%d | recognizedText= %s' 
                  % (response.resultCd, response.recognizedText))
            resultText = response.recognizedText
            break
        else:
            print('resultCd=%d | recognizedText= %s' 
                  % (response.resultCd, response.recognizedText))
    if len(resultText)==0:
        resultText = '다시 시도하십시오.'
    return resultText

# TTS : getText2VoiceStream
def getText2VoiceStream(inText,inFileName):

    channel = grpc.secure_channel('{}:{}'.format(HOST, PORT), getCredentials())
    stub = gigagenieRPC_pb2_grpc.GigagenieStub(channel)

    message = gigagenieRPC_pb2.reqText()
    message.lang=0
    message.mode=0
    message.text=inText
    writeFile=open(inFileName,'wb')
    for response in stub.getText2VoiceStream(message):
        if response.HasField("resOptions"):
            print ("ResVoiceResult: %d" %(response.resOptions.resultCd))
        if response.HasField("audioContent"):
            print ("Audio Stream")
            writeFile.write(response.audioContent)
    writeFile.close()
    player.load_audio(inFileName)
    player.play_audio()

import ktkws
KWSID = ['지니', '지니야', '친구야', '자기야']

def detect():
    
    with MicrophoneStream(RATE, CHUNK) as stream:
        audio_generator = stream.generator()

        for content in audio_generator:

            rc = ktkws.detect(content)
            #rms = audioop.rms(content,2)
            #print('audio rms = %d' % (rms))

            if (rc == 1):
                player.load_audio("../data/sample_yes.wav")
                player.play_audio()
                return 200
def convertDataToJson(data):
    fr = data.index("{")
    en = data.index("}") + 1
    str = data[fr:en]
    jdata = j.loads(str)
    return jdata

def on_connect(mqttc,obj,flags,rc):
    print("rc: " + str(rc))

def on_message(mqttc,obj,msg):
    jdata = convertDataToJson(str(msg.payload))
    if(msg.topic == SENT_USER_KEY):
        lesson = jdata["lesson"]
        user = jdata["uid"]
        sen_num = jdata["sen_num"]
        data = con_data[lesson]["kr"][user][sen_num]
        print(data)
        getText2VoiceStream(data,"./testtts.wav")
    else:
        print(jdata["status"])

def on_publish(mqttc,obj,mid):
    print("mind: " + str(mid))

def on_subscribe(mqttc,obj,mid,granted_qos):
    print("Subscribed: "+ str(mid)+" "+str(granted_qos))


if __name__ == '__main__':
   # ktkws.set_keyword(KWSID.index('친구야'))
   # rc = ktkws.init("../data/kwsmodel.pack")
    #rc = ktkws.start()
    #rc = detect()
    mqttc = mqtt.Client()
    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish
    mqttc.on_subscribe = on_subscribe
    mqttc.connect("test.mosquitto.org",1883,60)
    mqttc.subscribe(USER_RECEIVED_CONFIRM,0)
    mqttc.subscribe(SENT_USER_KEY,0)
    mqttc.loop_forever()
'''
    while True:
        if 1:
            #ktkws.stop()
            list_text = getVoice2Text()
            print(list_text)
            getText2VoiceStream(list_text,"./testtts.wav")
            for i in list_text:
                if i=='친구야':
                    getText2VoiceStream("제 이름은 Giga Genie입니다.","./testtts.wav")
                if i=='자기야':
                    getText2VoiceStream("나는 100이야.","./testtts.wav")
                if i=='어느 나라 사람이에요':
                    getText2VoiceStream("나는 은하 N231에서 왔어.","./testtts.wav")
            for i in list_text:
                print(i)'''
