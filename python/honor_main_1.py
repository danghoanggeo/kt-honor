#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-*- coding: utf-8 -*-
#-*- coding: uec-kr -*-

from __future__ import print_function

import grpc

import gigagenieRPC_pb2
import gigagenieRPC_pb2_grpc

import os
import datetime
import hmac
import hashlib

import context
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json as j
import re, time
import RPi.GPIO as GPIO
from threading import Thread
from _player import WavePlayer



USER_RECEIVED_CONFIRM ="mqtt/demo-kt-confirm"
TOPIC_USER_OK = "mqtt/user-speak-ok"
TOPIC_CONVERSATION = "honor/topic-conversation"
SUB_TOPIC_HONOR_CONVER_START = "honor/topic-con-start"
PUBLIC_HONOR_CONVER_START_OK = "honor/topic-con-start-ok"
PUB_TOPIC_USER_SPEAK_OK = "honor/topic-user-speak-ok"
HOSTNAME = "test.mosquitto.org"

# Config for GiGA Genie gRPC
CLIENT_ID = 'Y2xpZW50X2lkMTU0Nzk0MzU1NTM5MA=='
CLIENT_KEY = 'Y2xpZW50X2tleTE1NDc5NDM1NTUzOTA='
CLIENT_SECRET = 'Y2xpZW50X3NlY3JldDE1NDc5NDM1NTUzOTA='
HOST = 'gate.gigagenie.ai'
PORT = 4080

player = WavePlayer()
led_mode = 0
LedPin = 31
GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location
GPIO.setup(LedPin, GPIO.OUT)
GPIO.setwarnings(False)

with open('data_conver.json') as f:
    con_data = j.load(f)

def led_listen():
    global led_mode
    while True:
        while led_mode:
            GPIO.output(LedPin, GPIO.HIGH)
            time.sleep(0.2)
        while not led_mode:
            GPIO.output(LedPin, GPIO.LOW)
            time.sleep(0.2)

thread_led = Thread(target=led_listen)
thread_led.daemon = True
thread_led.start()

def getMetadata():
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")[:-3]
    message = CLIENT_ID + ':' + timestamp

    signature = hmac.new(CLIENT_SECRET.encode(), message.encode(), hashlib.sha256).hexdigest()

    metadata = [('x-auth-clientkey', CLIENT_KEY),
                ('x-auth-timestamp', timestamp),
                ('x-auth-signature', signature)]

    return metadata

def credentials(context, callback):
    callback(getMetadata(), None)

def getCredentials():
    with open('../data/ca-bundle.pem', 'rb') as f:
        trusted_certs = f.read()
    sslCred = grpc.ssl_channel_credentials(root_certificates=trusted_certs)

    authCred = grpc.metadata_call_credentials(credentials)

    return grpc.composite_channel_credentials(sslCred, authCred)

### END OF COMMON ###

### STT
import pyaudio
import audioop
from six.moves import queue

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
CHUNK = 512

# MicrophoneStream - original code in https://goo.gl/7Xy3TT
class MicrophoneStream(object):
    """Opens a recording stream as a generator yielding the audio chunks."""
    def __init__(self, rate, chunk):
        self._rate = rate
        self._chunk = chunk

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

    def __enter__(self):
        self._audio_interface = pyaudio.PyAudio()
        self._audio_stream = self._audio_interface.open(
            format=pyaudio.paInt16,
            channels=1, rate=self._rate,
            input=True, frames_per_buffer=self._chunk,
            # Run the audio stream asynchronously to fill the buffer object.
            # This is necessary so that the input device's buffer doesn't
            # overflow while the calling thread makes network requests, etc.
            stream_callback=self._fill_buffer,
        )

        self.closed = False

        return self

    def __exit__(self, type, value, traceback):
        self._audio_stream.stop_stream()
        self._audio_stream.close()
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        self._audio_interface.terminate()

    def _fill_buffer(self, in_data, frame_count, time_info, status_flags):
        """Continuously collect data from the audio stream, into the buffer."""
        self._buff.put(in_data)
        return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)
# [END audio_stream]

def generate_request():
    i = 0

    with MicrophoneStream(RATE, CHUNK) as stream:
        audio_generator = stream.generator()
    
        for content in audio_generator:
            i += 1
            message = gigagenieRPC_pb2.reqVoice()
            message.audioContent = content
            yield message
        print('Number of content: {}'.format(i))
def getVoice2Text():	
    global led_mode
    led_mode = 1
    print ("Ctrl+\ to quit ...")
	
    channel = grpc.secure_channel('{}:{}'.format(HOST, PORT), getCredentials())
    stub = gigagenieRPC_pb2_grpc.GigagenieStub(channel)
    request = generate_request()
    resultText = ''
    list_resp=[]
    count = 0
    for response in stub.getVoice2Text(request):
        if response.resultCd == 200:
        #if response.resultCd == 200: # partial
            print('resultCd=%d | recognizedText= %s' 
                  % (response.resultCd, response.recognizedText))
            resultText = response.recognizedText
            list_resp.append(resultText)
            count += 1
            if count == 3: break
        elif response.resultCd == 201: # final
            print('resultCd=%d | recognizedText= %s' 
                  % (response.resultCd, response.recognizedText))
            resultText = response.recognizedText
            list_resp.append(resultText)
            break
        else:
            print('resultCd=%d | recognizedText= %s' 
                  % (response.resultCd, response.recognizedText))
   # if len(resultText)==0:
    #    resultText = '다시 시도하십시오.'
    return list_resp


# TTS : getText2VoiceStream
def getText2VoiceStream(inText,inFileName):
    global led_mode
    led_mode = 0
    channel = grpc.secure_channel('{}:{}'.format(HOST, PORT), getCredentials())
    stub = gigagenieRPC_pb2_grpc.GigagenieStub(channel)

    message = gigagenieRPC_pb2.reqText()
    message.lang=0
    message.mode=0
    message.text=inText
    writeFile=open(inFileName,'wb')
    for response in stub.getText2VoiceStream(message):
        if response.HasField("resOptions"):
            print ("ResVoiceResult: %d" %(response.resOptions.resultCd))
        if response.HasField("audioContent"):
            print ("Audio Stream")
            writeFile.write(response.audioContent)
    writeFile.close()
    player.load_audio(inFileName)
    player.play_audio()

import ktkws
KWSID = ['지니', '지니야', '친구야', '자기야']

def detect():
    
    with MicrophoneStream(RATE, CHUNK) as stream:
        audio_generator = stream.generator()

        for content in audio_generator:

            rc = ktkws.detect(content)
            #rms = audioop.rms(content,2)
            #print('audio rms = %d' % (rms))

            if (rc == 1):
                player.load_audio("../data/sample_yes.wav")
                player.play_audio()
                return 200
def swichUser(user):
    if(user =="1"):
        return "2"
    else:
        return "1"
    
def compareSentence(userSpeaks, expectedSpeak):
    flag = False
    count_sen = 0
    expectedSpeak = expectedSpeak.replace(" ","")
    expectedSpeak = expectedSpeak.replace(".","")
    expectedSpeak = expectedSpeak.replace(",","")
    return_bestSen = ""
    print("expected: "+expectedSpeak)
    for userSpeak in userSpeaks :
        if userSpeak == None:
            continue
        userSpeak = userSpeak.replace(" ","")
        userSpeak = userSpeak.replace(".","")
        userSpeak = userSpeak.replace(",","")
        print("Your time: "+str(count_sen)+": "+userSpeak)
        if userSpeak == expectedSpeak :
            return ""
        else:
            count_sen = count_sen +1
        if(count_sen == 2):
            return userSpeak
    return "not recognize"
def publishTopic(topic,data):
    publish.single(topic,data,hostname = HOSTNAME)

def aiSpeakNextSentence(message):
    lesson = message["lesson"]
    ai_user = message["uid"]
    sen_num = message["sen_num"]
    print(ai_user)
    data = con_data[lesson]["kr"][ai_user][sen_num]
    print(data)
    if data:
        publishTopic(PUB_TOPIC_USER_SPEAK_OK,j.dumps({"content":data,"type":"say","uid":"0"}))
        getText2VoiceStream(data,"./testtts.wav")
        return False
    else:
        if(user =="2"):
            return True

def hoMain(message):
    lesson = message["lesson"]
    user = message["uid"]
    sen_num_user = 1
    sen_num_ai = 1
    user_data = con_data[lesson]["kr"][user][str(sen_num_user)]
    finish  = False
    ai_User = swichUser(user)
    nextSen = {"uid":ai_User,"lesson":lesson,"sen_num":str(sen_num_ai)}
    count = 0
    
    while True:
        list_text = getVoice2Text()
        print(list_text)
        compare = compareSentence(list_text,user_data)
        if compare == "":
            publishTopic(PUB_TOPIC_USER_SPEAK_OK,j.dumps({"content":user_data,"speak":"1","uid":user}))
            print("ai_sen_num: "+str(sen_num_ai))
            finish = aiSpeakNextSentence(nextSen)
            if finish:
                break
            sen_num_user = sen_num_user+1
            print("user_sen_num: "+str(sen_num_user))
            user_data = con_data[lesson]["kr"][user][str(sen_num_user)]
            print(user_data)
            if not user_data:
                break
            print("user expected sentence: "+user_data)
            sen_num_ai = sen_num_ai+1
            nextSen = {"uid":ai_User,"lesson":lesson,"sen_num":str(sen_num_ai)}
            count = 0
        else:
            publishTopic(PUB_TOPIC_USER_SPEAK_OK,j.dumps({"content":compare,"speak":"0","uid":user}))  
        if count <= 3:
            publishTopic(PUBLIC_HONOR_CONVER_START_OK,j.dumps({"status":1,"type":"repeat","content":"다시 말해보십시오.","uid":"0"}))
            getText2VoiceStream("화자의 문장을 말하십시오.","./testtts.wav")
        if count == 4:
            publishTopic(PUBLIC_HONOR_CONVER_START_OK,j.dumps({"status":1,"type":"sorry","content":"화자의 문장을","uid":"0"}))
            getText2VoiceStream("화자의 문장을 ","./testtts.wav")
            break
        count = count +1
        if(finish):
            publishTopic(PUBLIC_HONOR_CONVER_START_OK,j.dumps({"status":1,"type":"finish","content":"축하합니다","uid":"0"}))
            getText2VoiceStream("축하합니다","./testtts.wav")
            break;

def startConversation(jdata):
    lesson = jdata["lesson"]
    user = jdata["uid"]
    publishTopic(PUBLIC_HONOR_CONVER_START_OK,j.dumps({"status":1,"content":"시작합시다","uid":"0"}))
    getText2VoiceStream("시작합시다","./testtts.wav")
    hoMain(jdata); # Wait for user spkeak
    print("start ok")
    '''else:
        sen_num = jdata["sen_num"]
        data = con_data[lesson]["kr"][user][sen_num]
        getText2VoiceStream(data,"./testtts.wav")'''
        
def convertDataToJson(data):
    fr = data.index("{")
    en = data.index("}") + 1
    str = data[fr:en]
    jdata = j.loads(str)
    return jdata

def on_connect(mqttc,obj,flags,rc):
    print("rc: " + str(rc))

def on_message(mqttc,obj,msg):
    jdata = convertDataToJson(str(msg.payload))
    #print(msg.topic)
    '''if(msg.topic == SENT_USER_KEY):
        lesson = jdata["lesson"]
        user = jdata["uid"]
        sen_num = jdata["sen_num"]
        data = con_data[lesson]["kr"][user][sen_num]
        print(data)
        getText2VoiceStream(data,"./testtts.wav")'''
    if(msg.topic == TOPIC_CONVERSATION):
        speakNextSentence(jdata)
        print(jdata["status"])
    elif(msg.topic == SUB_TOPIC_HONOR_CONVER_START):
        print("start con")
        startConversation(jdata)

def on_publish(mqttc,obj,mid):
    print("mind: " + str(mid))

def on_subscribe(mqttc,obj,mid,granted_qos):
    print("Subscribed: "+ str(mid)+" "+str(granted_qos))


if __name__ == '__main__':
   # ktkws.set_keyword(KWSID.index('친구야'))
   # rc = ktkws.init("../data/kwsmodel.pack")
    #rc = ktkws.start()
    #rc = detect()
    mqttc = mqtt.Client()
    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish
    mqttc.on_subscribe = on_subscribe
    mqttc.connect(HOSTNAME,1883,60)
    mqttc.subscribe(USER_RECEIVED_CONFIRM,0)
    mqttc.subscribe(TOPIC_USER_OK,0)
    mqttc.subscribe(SUB_TOPIC_HONOR_CONVER_START,0)
    mqttc.loop_forever()
