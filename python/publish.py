import context
import paho.mqtt.publish as publish
import json

SENT_USER_KEY = "demo/mqtt-hack-ai"
sent_data = json.dumps({"uid":1,"lesson":1,"sen_num":0,"content":"안녕하세요. 저는 타완이에요."})

publish.single(SENT_USER_KEY,sent_data,hostname = "test.mosquitto.org")
